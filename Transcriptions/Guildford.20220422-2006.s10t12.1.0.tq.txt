       Transcription Difficulty Analysis
         Bell Handstroke Backstroke
           1     2.91        1.66
           2     0.17        0.33
           3     1.73        1.43
           4     0.59        0.33
           5     0.25        1.31
           6     0.86        1.50
           7     2.34        3.99
           8     2.04        1.66
           9     2.41        0.73
          10     13.60        5.94
         Overall Transcription Difficulty       2.29
       Bad Blow Analysis
         Reporting threshold = 39
         Total blow transcriptions exceeding threshold: 0
