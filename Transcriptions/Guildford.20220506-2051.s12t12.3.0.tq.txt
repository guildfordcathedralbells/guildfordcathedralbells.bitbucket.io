       Transcription Difficulty Analysis
         Bell Handstroke Backstroke
           1     3.15        5.52
           2     1.08        1.22
           3     2.70        2.84
           4     0.71        0.80
           5     1.29        1.65
           6     0.57        0.90
           7     1.60        2.06
           8     1.47        1.67
           9     1.32        1.44
          10     2.51        2.84
          11     2.39        2.86
          12     13.65        5.61
         Overall Transcription Difficulty       2.58
       Bad Blow Analysis
         Reporting threshold = 39
         List of blow transcriptions exceeding threshold
            ChangeNo  BellNo  metric
               47     12       40
               82     10       41
              106      1       47
              108      1       52
              153     12       41
              189      3       43
              247      1       57
              263     12       42
         Total blow transcriptions exceeding threshold: 8
