       Transcription Difficulty Analysis
         Bell Handstroke Backstroke
           1     3.84        5.61
           2     0.52        0.76
           3     4.43        2.36
           4     0.39        0.37
           5     1.11        1.59
           6     0.81        0.82
           7     1.41        2.04
           8     1.58        1.45
           9     2.20        2.00
          10     2.65        2.94
          11     3.43        7.19
          12     12.48        4.98
         Overall Transcription Difficulty       2.79
       Bad Blow Analysis
         Reporting threshold = 39
         List of blow transcriptions exceeding threshold
            ChangeNo  BellNo  metric
               32     11       45
               56     11       44
               60      1       49
               64     11       61
               83     12       54
              102     11       70
              111      1       39
              151     12       39
              164     11       42
              180     11       51
              182     11       47
              229     12       40
              246      1       52
         Total blow transcriptions exceeding threshold: 13
