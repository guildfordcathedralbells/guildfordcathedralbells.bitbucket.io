       Transcription Difficulty Analysis
         Bell Handstroke Backstroke
           1     11.13        12.03
           2     4.46        6.60
           3     1.64        1.83
           4     1.86        2.39
           5     5.13        6.00
           6     0.54        0.58
           7     1.74        1.80
           8     2.84        2.08
           9     2.61        3.04
          10     0.64        0.38
          11     0.04        1.24
          12     1.28        0.32
         Overall Transcription Difficulty       3.01
       Bad Blow Analysis
         Reporting threshold = 39
         List of blow transcriptions exceeding threshold
            ChangeNo  BellNo  metric
               18     11       39
               33      1       46
               74      1       39
               82     11       41
               98      1       47
               98     11       41
              107      1       54
              108      1       40
              113      1       70
              117      1       42
              126     11       79
              128     11       84
         Total blow transcriptions exceeding threshold: 12
