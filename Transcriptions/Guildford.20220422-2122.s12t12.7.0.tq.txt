       Transcription Difficulty Analysis
         Bell Handstroke Backstroke
           1     4.47        6.23
           2     0.60        0.83
           3     4.42        2.45
           4     0.32        0.41
           5     1.17        1.66
           6     0.62        0.80
           7     1.55        2.21
           8     1.56        1.48
           9     1.74        1.74
          10     2.68        2.99
          11     3.39        6.00
          12     13.14        5.12
         Overall Transcription Difficulty       2.82
       Bad Blow Analysis
         Reporting threshold = 39
         List of blow transcriptions exceeding threshold
            ChangeNo  BellNo  metric
               52     11       49
               54      1       57
               55      1       58
               88     11       43
               95     12       44
              109      1       82
              116      1       81
              160      1       42
              184      1       43
              213     12       45
              242      1       44
         Total blow transcriptions exceeding threshold: 11
