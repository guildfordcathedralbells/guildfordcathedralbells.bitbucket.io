#. Lowndes: Version 2
#. SourceFilename: W:\Recordings\Guildford\Guildford.20220610-2115.s12t12.2.wav
#. Tower: Guildford Cathedral
#. RingingName: Touch_2
#. RingingDateTime: 20220610-2115
#. Rungon: 
#. Tenor: 12
#. NumBellsRinging: 12
#. Creator: HawkEar Team Edition V1 for Guildford Cathedral
#. CreatorVersion: 1.1.0.3
#. TranscribedFor: Guildford band
#. TranscriptionDateTime: 20220613-1230
#. FirstBlowMs: 13170
#. TouchdB: -0.100243
#. TraindB: -9.73, -8.5, -6.1, -3.11, -3.16, -3.1, -3.98, -4.12, -5.95, -5.43, -4.92, -3
#. TrainedOn: 20220102-1438
#. TrainDataRecordedOn: 20210813-1741
#. TrainParams: -ft:y -ws:0.05 -hs:0.01 -co:0.9 -sw:40 -ts:
#. GainParams: -fh:6000 -fl:200
#. OnsetParams: -mb:80 -mf:10 -ng:0.3 -oo:0
#. WriteParams: -hb:y
#. AnalParams: -au:n -xp:n -sg:n -hsg:-1
#. ExpString: 
#. Association:
#. Calling:
#. CompositionName:
#. Method:
#. Footnotes:
#. Ringers:
#. Stage:
H 1 0X0000
H 2 0X00be
H 3 0X01a4
H 4 0X02d0
H 5 0X03b6
H 6 0X0456
H 7 0X051e
H 8 0X05f0
H 9 0X06cc
H O 0X07c6
H E 0X08b6
H T 0X09a6
B 1 0X0aa0
B 2 0X0b4a
B 3 0X0c1c
B 4 0X0cee
B 5 0X0d84
B 6 0X0e7e
B 7 0X0f0a
B 8 0X0fe6
B 9 0X10e0
B O 0X11b2
B E 0X12d4
B T 0X13c4
H 1 0X157c
H 2 0X163a
H 3 0X16c6
H 4 0X1784
H 5 0X1838
H 6 0X18d8
H 7 0X19d2
H 8 0X1a90
H 9 0X1b80
H O 0X1c70
H E 0X1d24
H T 0X1e0a
B 1 0X1f04
B 2 0X1fcc
B 3 0X2094
B 4 0X2134
B 5 0X21f2
B 6 0X22ce
B 7 0X2382
B 8 0X244a
B 9 0X253a
B O 0X2602
B E 0X26f2
B T 0X27b0
H 1 0X29a4
H 2 0X2a76
H 3 0X2b20
H 4 0X2bf2
H 5 0X2c88
H 6 0X2d6e
H 7 0X2e2c
H 8 0X2ec2
H 9 0X2fbc
H O 0X30a2
H E 0X3138
H T 0X3228
B 1 0X32f0
B 2 0X33b8
B 3 0X3480
B 4 0X3548
B 5 0X3610
B 6 0X36ec
B 7 0X37b4
B 8 0X3872
B 9 0X394e
B O 0X3a2a
B E 0X3b38
B T 0X3bb0
H 1 0X3dae
H 2 0X3e3a
H 3 0X3ef8
H 4 0X3fc0
H 5 0X406a
H 6 0X413c
H 7 0X4222
H 8 0X42ea
H 9 0X43c6
H O 0X44ca
H E 0X454c
H T 0X4628
B 1 0X4704
B 2 0X47cc
B 3 0X488a
B 4 0X492a
B 5 0X49fc
B 6 0X4ac4
B 7 0X4ba0
B 8 0X4c54
B 9 0X4d4e
B O 0X4e34
B E 0X4f1a
B T 0X4fce
H 1 0X517c
H 2 0X521c
H 3 0X52da
H 4 0X53ac
H 5 0X544c
H 6 0X553c
H 7 0X5622
H 8 0X56e0
H 9 0X57a8
H O 0X58a2
H E 0X5956
H T 0X5a1e
B 1 0X5af0
B 2 0X5ba4
B 3 0X5c62
B 4 0X5d16
B 5 0X5dd4
B 6 0X5ea6
B 7 0X5f82
B 8 0X6054
B 9 0X6126
B O 0X6202
B E 0X62ca
B T 0X63a6
H 1 0X659a
H 2 0X6630
H 3 0X66e4
H 4 0X678e
H 5 0X681a
H 6 0X6900
H 7 0X69fa
H 8 0X6aae
H 9 0X6b76
H O 0X6c70
H E 0X6d1a
H T 0X6dce
B 1 0X6ec8
B 2 0X6f9a
B 3 0X7044
B 4 0X7102
B 5 0X71c0
B 6 0X72a6
B 7 0X7350
B 8 0X742c
B 9 0X74fe
B O 0X75f8
B E 0X76ac
B T 0X7774
H 2 0X7940
H 1 0X7a30
H 3 0X7abc
H 5 0X7b7a
H 4 0X7c74
H 7 0X7d28
H 6 0X7df0
H 9 0X7ed6
H 8 0X7f8a
H E 0X803e
H O 0X814c
H T 0X81ec
B 2 0X82aa
B 3 0X8368
B 1 0X8444
B 4 0X8502
B 5 0X85ca
B 6 0X86b0
B 7 0X875a
B 8 0X882c
B 9 0X891c
B O 0X89ee
B E 0X8aac
B T 0X8b7e
H 3 0X8d2c
H 2 0X8df4
H 4 0X8ea8
H 1 0X8f8e
H 6 0X9060
H 5 0X9114
H 8 0X91c8
H 7 0X92a4
H O 0X938a
H 9 0X9434
H E 0X94e8
H T 0X95ce
B 2 0X96dc
B 3 0X97a4
B 4 0X984e
B 6 0X990c
B 1 0X9a06
B 8 0X9a88
B 5 0X9b50
B O 0X9c4a
B 7 0X9cea
B E 0X9dd0
B 9 0X9eac
B T 0X9f60
H 2 0Xa10e
H 4 0Xa1fe
H 3 0Xa2bc
H 1 0Xa3a2
H 6 0Xa42e
H 5 0Xa50a
H 8 0Xa5dc
H 7 0Xa690
H O 0Xa762
H 9 0Xa816
H E 0Xa8de
H T 0Xa9a6
B 4 0Xaab4
B 2 0Xab68
B 3 0Xac26
B 6 0Xacf8
B 1 0Xade8
B 8 0Xae88
B 5 0Xaf46
B O 0Xb040
B 7 0Xb0d6
B E 0Xb216
B 9 0Xb284
B T 0Xb34c
H 4 0Xb536
H 3 0Xb5ea
H 2 0Xb68a
H 1 0Xb78e
H 6 0Xb81a
H 5 0Xb8d8
H 8 0Xb9b4
H 7 0Xbaa4
H O 0Xbb62
H 9 0Xbc16
H E 0Xbcca
H T 0Xbda6
B 3 0Xbeb4
B 4 0Xbf86
B 2 0Xc04e
B 6 0Xc102
B 1 0Xc1d4
B 8 0Xc288
B 5 0Xc33c
B O 0Xc418
B 7 0Xc4ea
B E 0Xc5b2
B 9 0Xc666
B T 0Xc756
H 4 0Xc918
H 3 0Xc9d6
H 6 0Xcaa8
H 2 0Xcba2
H 8 0Xcc4c
H 1 0Xcd50
H O 0Xcdfa
H 5 0Xceae
H E 0Xcf58
H 7 0Xd034
H 9 0Xd0fc
H T 0Xd1c4
B 4 0Xd282
B 6 0Xd35e
B 3 0Xd43a
B 8 0Xd4f8
B 2 0Xd606
B O 0Xd6ce
B 1 0Xd796
B E 0Xd822
B 5 0Xd908
B 9 0Xd9ee
B 7 0Xdac0
B T 0Xdb6a
H 6 0Xdd4a
H 4 0Xde30
H 3 0Xdeee
H 2 0Xdfca
H 8 0Xe06a
H 1 0Xe146
H O 0Xe222
H 5 0Xe2d6
H E 0Xe36c
H 7 0Xe452
H 9 0Xe524
H T 0Xe5ce
B 6 0Xe6dc
B 3 0Xe7b8
B 4 0Xe89e
B 8 0Xe934
B 2 0Xea2e
B O 0Xead8
B 1 0Xeb78
B E 0Xec4a
B 5 0Xed3a
B 9 0Xee20
B 7 0Xeefc
B T 0Xef92
H 3 0Xf15e
H 6 0Xf226
H 4 0Xf2ee
H 2 0Xf3fc
H 8 0Xf4a6
H 1 0Xf578
H O 0Xf654
H 5 0Xf6f4
H E 0Xf776
H 7 0Xf848
H 9 0Xf92e
H T 0Xfa00
B 3 0Xfafa
B 4 0Xfba4
B 6 0Xfc6c
B 8 0Xfd52
B 2 0Xfe60
B O 0Xff14
B 1 0Xffc8
B E 0X007c
B 5 0X0130
B 9 0X0234
B 7 0X02de
B T 0X03ba
H 4 0X0540
H 3 0X0608
H 8 0X06f8
H 6 0X07e8
H O 0X08ec
H 2 0X09b4
H E 0X0a40
H 1 0X0b30
H 9 0X0bc6
H 5 0X0c84
H 7 0X0d56
H T 0X0e28
B 3 0X0f22
B 4 0X0fc2
B 8 0X1076
B O 0X1184
B 6 0X122e
B E 0X131e
B 2 0X140e
B 9 0X14b8
B 1 0X15d0
B 7 0X1666
B 5 0X16fc
B T 0X17ce
H 3 0X19ae
H 8 0X1a6c
H 4 0X1b48
H 6 0X1bfc
H O 0X1d00
H 2 0X1db4
H E 0X1e68
H 1 0X1f76
H 9 0X1fe4
H 5 0X20b6
H 7 0X217e
H T 0X2278
B 8 0X2340
B 3 0X2430
B 4 0X24ee
B O 0X25b6
B 6 0X2688
B E 0X2764
B 2 0X2818
B 9 0X28c2
B 1 0X29c6
B 7 0X2a70
B 5 0X2b24
B T 0X2c28
H 8 0X2db8
H 4 0X2ebc
H 3 0X2f7a
H 6 0X3042
H O 0X3132
H 2 0X320e
H E 0X3290
H 1 0X3358
H 9 0X340c
H 5 0X34d4
H 7 0X35ba
H T 0X3696
B 4 0X375e
B 8 0X383a
B 3 0X392a
B O 0X3a06
B 6 0X3ab0
B E 0X3bbe
B 2 0X3c54
B 9 0X3d30
B 1 0X3dbc
B 7 0X3eac
B 5 0X3f56
B T 0X405a
H 8 0X421c
H 4 0X42ee
H O 0X43fc
H 3 0X449c
H E 0X4546
H 6 0X4622
H 9 0X46e0
H 2 0X47da
H 7 0X487a
H 5 0X49d8
H 1 0X4a8c
H T 0X4aaa
B 8 0X4b7c
B O 0X4c80
B 4 0X4d8e
B E 0X4e2e
B 3 0X4eec
B 9 0X4fc8
B 6 0X50ae
B 7 0X5158
B 2 0X5252
B 5 0X52fc
B 1 0X53ec
B T 0X5496
H O 0X5644
H 8 0X56da
H 4 0X57d4
H 3 0X58a6
H E 0X5982
H 6 0X5a40
H 9 0X5afe
H 2 0X5c16
H 7 0X5cc0
H 1 0X5dd8
H 5 0X5e5a
H T 0X5f36
B O 0X5fe0
B 4 0X6094
B 8 0X6152
B E 0X626a
B 3 0X635a
B 9 0X63f0
B 6 0X64e0
B 7 0X65a8
B 2 0X6666
B 5 0X6742
B 1 0X6832
B T 0X68dc
H 4 0X6ab2
H O 0X6b84
H 8 0X6c10
H 3 0X6d00
H E 0X6db4
H 6 0X6e5e
H 9 0X6f4e
H 2 0X705c
H 7 0X7110
H 1 0X71f6
H 5 0X72aa
H T 0X7372
B 4 0X7430
B 8 0X750c
B O 0X75d4
B E 0X76e2
B 3 0X77aa
B 9 0X784a
B 6 0X7930
B 7 0X79ee
B 2 0X7aca
B 5 0X7b74
B 1 0X7c6e
B T 0X7d2c
H 8 0X7f02
H 4 0X7fde
H E 0X8088
H O 0X8182
H 9 0X8218
H 3 0X82fe
H 7 0X83c6
H 6 0X848e
H 5 0X8538
H 2 0X8628
H 1 0X8704
H T 0X87b8
B 4 0X88bc
B 8 0X8984
B E 0X8a56
B 9 0X8af6
B O 0X8baa
B 7 0X8ca4
B 3 0X8d76
B 5 0X8e34
B 6 0X8f24
B 1 0X8fce
B 2 0X9096
B T 0X914a
H 4 0X9398
H E 0X9406
H 8 0X94ba
H O 0X95b4
H 9 0X967c
H 3 0X9744
H 7 0X980c
H 6 0X98b6
H 5 0X9988
H 2 0X9a50
H 1 0X9b36
H T 0X9bd6
B E 0X9cee
B 4 0X9dde
B 8 0X9e92
B 9 0X9f5a
B O 0Xa022
B 7 0Xa0e0
B 3 0Xa1c6
B 5 0Xa284
B 6 0Xa32e
B 1 0Xa3d8
B 2 0Xa4aa
B T 0Xa586
H E 0Xa752
H 8 0Xa82e
H 4 0Xa928
H O 0Xa9fa
H 9 0Xaacc
H 3 0Xab8a
H 7 0Xac8e
H 6 0Xad10
H 5 0Xadd8
H 2 0Xaeb4
H 1 0Xaf7c
H T 0Xb01c
B 8 0Xb102
B E 0Xb1fc
B 4 0Xb2b0
B 9 0Xb3b4
B O 0Xb47c
B 7 0Xb54e
B 3 0Xb62a
B 5 0Xb6ca
B 6 0Xb7b0
B 1 0Xb85a
B 2 0Xb90e
B T 0Xb9e0
H E 0Xbba2
H 8 0Xbc74
H 9 0Xbd50
H 4 0Xbe40
H 7 0Xbf26
H O 0Xbfee
H 5 0Xc08e
H 3 0Xc14c
H 1 0Xc250
H 6 0Xc30e
H 2 0Xc3c2
H T 0Xc480
B E 0Xc5a2
B 9 0Xc656
B 8 0Xc714
B 7 0Xc80e
B 4 0Xc8d6
B 5 0Xc976
B O 0Xca48
B 1 0Xcb42
B 3 0Xcbec
B 2 0Xccbe
B 6 0Xcd9a
B T 0Xce6c
H 9 0Xcfde
H E 0Xd0ce
H 8 0Xd18c
H 4 0Xd29a
H 7 0Xd380
H O 0Xd43e
H 5 0Xd4de
H 3 0Xd5b0
H 1 0Xd66e
H 6 0Xd754
H 2 0Xd7f4
H T 0Xd8e4
B 9 0Xd9ac
B 8 0Xda88
B E 0Xdb64
B 7 0Xdc54
B 4 0Xdd12
B 5 0Xddc6
B O 0Xde84
B 1 0Xdf56
B 3 0Xe028
B 2 0Xe118
B 6 0Xe1d6
B T 0Xe2a8
H 8 0Xe442
H 9 0Xe50a
H E 0Xe604
H 4 0Xe6ea
H 7 0Xe7da
H O 0Xe898
H 5 0Xe942
H 3 0Xe9f6
H 1 0Xea96
H 6 0Xeb86
H 2 0Xec58
H T 0Xed0c
B 8 0Xedd4
B E 0Xeed8
B 9 0Xefbe
B 7 0Xf0b8
B 4 0Xf16c
B 5 0Xf22a
B O 0Xf2f2
B 1 0Xf3c4
B 3 0Xf45a
B 2 0Xf568
B 6 0Xf630
B T 0Xf6da
H E 0Xf8b0
H 8 0Xf978
H 7 0Xfa90
H 9 0Xfb1c
H 5 0Xfbee
H 4 0Xfcde
H 1 0Xfda6
H O 0Xfe50
H 2 0Xff22
H 3 0Xffcc
H 6 0X008a
H T 0X018e
B 8 0X0242
B E 0X0350
B 7 0X0404
B 5 0X04e0
B 9 0X058a
B 1 0X067a
B 4 0X0742
B 2 0X0832
B O 0X08b4
B 6 0X09ae
B 3 0X0a4e
B T 0X0b34
H 8 0X0cec
H 7 0X0e04
H E 0X0ea4
H 9 0X0f58
H 5 0X105c
H 4 0X114c
H 1 0X1200
H O 0X12b4
H 2 0X1354
H 3 0X1412
H 6 0X150c
H T 0X15d4
B 7 0X16d8
B 8 0X178c
B E 0X1868
B 5 0X1958
B 9 0X1a02
B 1 0X1ade
B 4 0X1ba6
B 2 0X1c82
B O 0X1d0e
B 6 0X1df4
B 3 0X1e9e
B T 0X1f84
H 7 0X2150
H 8 0X216e
H E 0X222c
H 9 0X238a
H 5 0X247a
H 4 0X2588
H 1 0X2682
H O 0X26fa
H 2 0X27cc
H 3 0X2876
H 6 0X295c
H T 0X2a10
B E 0X2b00
B 7 0X2bbe
B 8 0X2c54
B 5 0X2d3a
B 9 0X2e20
B 1 0X2f6a
B 4 0X2fe2
B 2 0X30fa
B O 0X3172
B 6 0X3244
B 3 0X3316
B T 0X33de
H 7 0X3582
H E 0X365e
H 5 0X3726
H 8 0X37ee
H 1 0X3960
H 9 0X39c4
H 2 0X3aaa
H 4 0X3b7c
H 6 0X3c1c
H O 0X3cc6
H 3 0X3d8e
H T 0X3eb0
B 7 0X3f32
B 5 0X402c
B E 0X40ea
B 1 0X420c
B 8 0X42ca
B 2 0X43a6
B 9 0X445a
B 6 0X4536
B 4 0X4612
B 3 0X46bc
B O 0X4770
B T 0X4838
H 5 0X4a0e
H 7 0X4acc
H E 0X4b94
H 8 0X4c70
H 1 0X4d88
H 9 0X4e00
H 2 0X4ee6
H 4 0X4fea
H 6 0X5094
H O 0X515c
H 3 0X51fc
H T 0X52ec
B 5 0X53c8
B E 0X549a
B 7 0X5558
B 1 0X5652
B 8 0X56fc
B 2 0X57ce
B 9 0X58a0
B 6 0X599a
B 4 0X5a6c
B 3 0X5b20
B O 0X5bb6
B T 0X5c92
H E 0X5e4a
H 5 0X5f26
H 7 0X5fee
H 8 0X60de
H 1 0X61d8
H 9 0X6278
H 2 0X6318
H 4 0X644e
H 6 0X64e4
H O 0X65d4
H 3 0X667e
H T 0X673c
B E 0X6868
B 7 0X6908
B 5 0X69a8
B 1 0X6ac0
B 8 0X6b74
B 2 0X6c46
B 9 0X6d04
B 6 0X6dcc
B 4 0X6eb2
B 3 0X6f84
B O 0X702e
B T 0X70ec
H 7 0X72ae
H E 0X7376
H 1 0X7470
H 5 0X7542
H 2 0X761e
H 8 0X76d2
H 6 0X77a4
H 9 0X7858
H 3 0X792a
H 4 0X7a06
H O 0X7b0a
H T 0X7ba0
B E 0X7ca4
B 7 0X7d58
B 1 0X7e48
B 2 0X7ef2
B 5 0X7fb0
B 6 0X80a0
B 8 0X8172
B 3 0X824e
B 9 0X82e4
B O 0X83ca
B 4 0X84a6
B T 0X856e
H E 0X8726
H 1 0X8820
H 7 0X88d4
H 5 0X89a6
H 2 0X8a82
H 8 0X8b36
H 6 0X8c1c
H 9 0X8cc6
H 3 0X8d84
H 4 0X8e60
H O 0X8f3c
H T 0X8fdc
B 1 0X90ea
B E 0X91c6
B 7 0X92ac
B 2 0X937e
B 5 0X941e
B 6 0X9518
B 8 0X95ea
B 3 0X9694
B 9 0X975c
B O 0X97f2
B 4 0X98f6
B T 0X99b4
H 1 0X9b80
H 7 0X9c5c
H E 0X9d2e
H 5 0X9e00
H 2 0X9ec8
H 8 0X9fa4
H 6 0Xa04e
H 9 0Xa12a
H 3 0Xa1fc
H 4 0Xa2b0
H O 0Xa3be
H T 0Xa440
B 7 0Xa53a
B 1 0Xa616
B E 0Xa706
B 2 0Xa7a6
B 5 0Xa864
B 6 0Xa940
B 8 0Xaa1c
B 3 0Xaae4
B 9 0Xab8e
B O 0Xac74
B 4 0Xad46
B T 0Xae18
H 1 0Xafe4
H 7 0Xb0c0
H 2 0Xb14c
H E 0Xb23c
H 6 0Xb2fa
H 5 0Xb3e0
H 3 0Xb49e
H 8 0Xb598
H O 0Xb656
H 9 0Xb700
H 4 0Xb7be
H T 0Xb8ae
B 1 0Xb94e
B 2 0Xba34
B 7 0Xbb1a
B 6 0Xbbe2
B E 0Xbd0e
B 3 0Xbd90
B 5 0Xbe4e
B O 0Xbf52
B 8 0Xbffc
B 4 0Xc0a6
B 9 0Xc1a0
B T 0Xc268
H 2 0Xc3ee
H 1 0Xc4de
H 7 0Xc59c
H E 0Xc68c
H 6 0Xc740
H 5 0Xc844
H 3 0Xc8ee
H 8 0Xc9e8
H O 0Xcaa6
H 9 0Xcb3c
H 4 0Xcc2c
H T 0Xcce0
B 2 0Xcdbc
B 7 0Xce8e
B 1 0Xcf56
B 6 0Xd064
B E 0Xd10e
B 3 0Xd1e0
B 5 0Xd2bc
B O 0Xd398
B 8 0Xd460
B 4 0Xd500
B 9 0Xd5d2
B T 0Xd6c2
H 7 0Xd85c
H 2 0Xd942
H 1 0Xda32
H E 0Xdae6
H 6 0Xdb9a
H 5 0Xdc8a
H 3 0Xdd52
H 8 0Xde2e
H O 0Xdf14
H 9 0Xdfaa
H 4 0Xe09a
H T 0Xe14e
B 7 0Xe22a
B 1 0Xe31a
B 2 0Xe3ba
B 6 0Xe4a0
B E 0Xe57c
B 3 0Xe658
B 5 0Xe702
B O 0Xe810
B 8 0Xe8c4
B 4 0Xe9aa
B 9 0Xea4a
B T 0Xeb08
H 1 0Xecd4
H 7 0Xed9c
H 6 0Xee82
H 2 0Xef86
H 3 0Xf044
H E 0Xf0e4
H O 0Xf1de
H 5 0Xf29c
H 4 0Xf3dc
H 8 0Xf45e
H 9 0Xf4fe
H T 0Xf5c6
B 7 0Xf666
B 1 0Xf74c
B 6 0Xf81e
B 3 0Xf936
B 2 0Xf9ea
B O 0Xfa9e
B E 0Xfb7a
B 4 0Xfc88
B 5 0Xfd32
B 9 0Xfe04
B 8 0Xfef4
B T 0Xffbc
H 7 0X011a
H 6 0X01f6
H 1 0X02e6
H 2 0X03b8
H 3 0X0462
H E 0X053e
H O 0X0656
H 5 0X06ec
H 4 0X07f0
H 8 0X08d6
H 9 0X0976
H T 0X0a5c
B 6 0X0af2
B 7 0X0bb0
B 1 0X0cd2
B 3 0X0d68
B 2 0X0e30
B O 0X0ef8
B E 0X1006
B 4 0X10ba
B 5 0X1196
B 9 0X125e
B 8 0X133a
B T 0X142a
H 6 0X1592
H 1 0X1696
H 7 0X175e
H 2 0X1830
H 3 0X18d0
H E 0X198e
H O 0X1a7e
H 5 0X1b6e
H 4 0X1c54
H 8 0X1d12
H 9 0X1d9e
H T 0X1e98
B 1 0X1f74
B 6 0X2046
B 7 0X2154
B 3 0X2208
B 2 0X22bc
B O 0X235c
B E 0X242e
B 4 0X2532
B 5 0X25f0
B 9 0X26ae
B 8 0X276c
B T 0X2870
H 6 0X2a0a
H 1 0X2ac8
H 3 0X2ba4
H 7 0X2cc6
H O 0X2dac
H 2 0X2e2e
H 4 0X2f14
H E 0X2faa
H 9 0X305e
H 5 0X3144
H 8 0X31f8
H T 0X32de
B 6 0X3400
B 3 0X34dc
B 1 0X3586
B O 0X364e
B 7 0X3716
B 4 0X3810
B 2 0X38e2
B 9 0X396e
B E 0X3a72
B 8 0X3b3a
B 5 0X3bee
B T 0X3cd4
H 3 0X3eb4
H 6 0X3f4a
H 1 0X401c
H 7 0X40da
H O 0X41de
H 2 0X42d8
H 4 0X43a0
H E 0X444a
H 9 0X44f4
H 5 0X45bc
H 8 0X4698
H T 0X4788
B 3 0X4864
B 1 0X4922
B 6 0X49ae
B O 0X4a9e
B 7 0X4b7a
B 4 0X4c42
B 2 0X4d46
B 9 0X4e04
B E 0X4f1c
B 8 0X4f94
B 5 0X5066
B T 0X5188
H 1 0X52fa
H 3 0X53b8
H 6 0X5458
H 7 0X557a
H O 0X5642
H 2 0X56f6
H 4 0X57b4
H E 0X5890
H 9 0X5962
H 5 0X5a34
H 8 0X5afc
H T 0X5c0a
B 1 0X5cbe
B 6 0X5d72
B 3 0X5e62
B O 0X5f0c
B 7 0X5fca
B 4 0X607e
B 2 0X616e
B 9 0X624a
B E 0X633a
B 8 0X640c
B 5 0X64fc
B T 0X65ce
H 6 0X674a
H 1 0X6826
H O 0X68f8
H 3 0X69b6
H 4 0X6aa6
H 7 0X6ba0
H 9 0X6c36
H 2 0X6d08
H 8 0X6dd0
H E 0X6ea2
H 5 0X6f74
H T 0X7064
B 1 0X710e
B 6 0X7208
B O 0X72b2
B 4 0X7398
B 3 0X7474
B 9 0X753c
B 7 0X7622
B 8 0X76e0
B 2 0X77bc
B 5 0X787a
B E 0X7942
B T 0X7a14
H 1 0X7bd6
H O 0X7cbc
H 6 0X7d3e
H 3 0X7e2e
H 4 0X7eec
H 7 0X800e
H 9 0X80cc
H 2 0X818a
H 8 0X8252
H E 0X831a
H 5 0X83e2
H T 0X84b4
B O 0X8590
B 1 0X8658
B 6 0X873e
B 4 0X87f2
B 3 0X88d8
B 9 0X89a0
B 7 0X8a72
B 8 0X8b30
B 2 0X8c16
B 5 0X8cf2
B E 0X8dd8
B T 0X8e64
H O 0X9026
H 6 0X90da
H 1 0X91c0
H 3 0X9288
H 4 0X9364
H 7 0X945e
H 9 0X94f4
H 2 0X95da
H 8 0X96ac
H E 0X9760
H 5 0X983c
H T 0X9936
B 6 0X9a08
B O 0X9ab2
B 1 0X9b7a
B 4 0X9c42
B 3 0X9d32
B 9 0X9e04
B 7 0X9ec2
B 8 0X9f8a
B 2 0Xa066
B 5 0Xa156
B E 0Xa1c4
B T 0Xa2d2
H O 0Xa4b2
H 6 0Xa566
H 4 0Xa642
H 1 0Xa700
H 9 0Xa80e
H 3 0Xa8a4
H 8 0Xa98a
H 7 0Xaa66
H 5 0Xaafc
H 2 0Xabec
H E 0Xac6e
H T 0Xad68
B O 0Xae44
B 4 0Xaf48
B 6 0Xaffc
B 9 0Xb0ce
B 1 0Xb1b4
B 8 0Xb27c
B 3 0Xb33a
B 5 0Xb3ee
B 7 0Xb4f2
B E 0Xb5b0
B 2 0Xb63c
B T 0Xb72c
H 4 0Xb93e
H O 0Xba06
H 6 0Xba92
H 1 0Xbb78
H 9 0Xbc4a
H 3 0Xbd12
H 8 0Xbdda
H 7 0Xbec0
H 5 0Xbf74
H 2 0Xc064
H E 0Xc0c8
H T 0Xc1a4
B 4 0Xc2c6
B 6 0Xc3a2
B O 0Xc44c
B 9 0Xc546
B 1 0Xc604
B 8 0Xc6b8
B 3 0Xc7a8
B 5 0Xc884
B 7 0Xc956
B E 0Xca1e
B 2 0Xcaf0
B T 0Xcb90
H 6 0Xcd48
H 4 0Xce2e
H O 0Xcf1e
H 1 0Xcfc8
H 9 0Xd0ae
H 3 0Xd180
H 8 0Xd252
H 7 0Xd338
H 5 0Xd3ce
H 2 0Xd4be
H E 0Xd522
H T 0Xd5fe
B 6 0Xd73e
B O 0Xd806
B 4 0Xd8ba
B 9 0Xd996
B 1 0Xda68
B 8 0Xdb1c
B 3 0Xdc16
B 5 0Xdcca
B 7 0Xddba
B E 0Xde64
B 2 0Xdf40
B T 0Xdfe0
H O 0Xe1de
H 6 0Xe29c
H 9 0Xe378
H 4 0Xe45e
H 8 0Xe526
H 1 0Xe5a8
H 5 0Xe6a2
H 3 0Xe77e
H E 0Xe81e
H 7 0Xe904
H 2 0Xe9fe
H T 0Xea94
B 6 0Xeba2
B O 0Xec60
B 9 0Xed3c
B 8 0Xee04
B 4 0Xeecc
B 5 0Xef8a
B 1 0Xf07a
B E 0Xf110
B 3 0Xf1d8
B 2 0Xf2c8
B 7 0Xf3a4
B T 0Xf476
H 6 0Xf5ac
H 9 0Xf700
H O 0Xf7dc
H 4 0Xf85e
H 8 0Xf94e
H 1 0Xfa48
H 5 0Xfad4
H 3 0Xfbba
H E 0Xfc32
H 7 0Xfd40
H 2 0Xfdea
H T 0Xfef8
B 9 0Xffde
B 6 0X00b0
B O 0X0182
B 8 0X024a
B 4 0X02fe
B 5 0X03d0
B 1 0X0498
B E 0X057e
B 3 0X063c
B 2 0X0718
B 7 0X07e0
B T 0X08a8
H 9 0X0a88
H O 0X0b46
H 6 0X0bfa
H 4 0X0d08
H 8 0X0dda
H 1 0X0eac
H 5 0X0f42
H 3 0X101e
H E 0X10b4
H 7 0X117c
H 2 0X1276
H T 0X1352
B O 0X1406
B 9 0X150a
B 6 0X15d2
B 8 0X16b8
B 4 0X176c
B 5 0X182a
B 1 0X18d4
B E 0X19ec
B 3 0X1a96
B 2 0X1b86
B 7 0X1c30
B T 0X1d02
H 9 0X1ef6
H O 0X1fb4
H 8 0X2086
H 6 0X214e
H 5 0X220c
H 4 0X22e8
H E 0X23a6
H 1 0X24b4
H 2 0X2540
H 3 0X25d6
H 7 0X26d0
H T 0X27ac
B 9 0X286a
B 8 0X296e
B O 0X2a2c
B 5 0X2af4
B 6 0X2bee
B E 0X2cb6
B 4 0X2d7e
B 2 0X2e5a
B 1 0X2f36
B 7 0X2fb8
B 3 0X3076
B T 0X3170
H 8 0X3332
H 9 0X33f0
H O 0X34d6
H 6 0X35bc
H 5 0X3666
H 4 0X372e
H E 0X37ba
H 1 0X38a0
H 2 0X399a
H 3 0X3a4e
H 7 0X3b20
H T 0X3bfc
B 8 0X3ce2
B O 0X3dbe
B 9 0X3ec2
B 5 0X3f76
B 6 0X4034
B E 0X4106
B 4 0X4192
B 2 0X4282
B 1 0X4354
B 7 0X43fe
B 3 0X44ee
B T 0X45b6
H O 0X4796
H 8 0X482c
H 9 0X491c
H 6 0X4a2a
H 5 0X4afc
H 4 0X4b92
H E 0X4c6e
H 1 0X4cd2
H 2 0X4dcc
H 3 0X4e8a
H 7 0X4f5c
H T 0X5024
B O 0X5146
B 9 0X51fa
B 8 0X52cc
B 5 0X53da
B 6 0X5498
B E 0X5560
B 4 0X560a
B 2 0X56e6
B 1 0X57a4
B 7 0X5858
B 3 0X590c
B T 0X59d4
H 9 0X5bb4
H O 0X5cae
H 5 0X5d8a
H 8 0X5e52
H E 0X5f1a
H 6 0X5fc4
H 2 0X60aa
H 4 0X61ae
H 7 0X6244
H 1 0X6352
H 3 0X63c0
H T 0X6488
B O 0X6564
B 9 0X667c
B 5 0X674e
B E 0X67f8
B 8 0X68f2
B 2 0X69c4
B 6 0X6a3c
B 7 0X6b4a
B 4 0X6c1c
B 3 0X6cf8
B 1 0X6de8
B T 0X6e6a
H O 0X7018
H 5 0X70fe
H 9 0X71bc
H 8 0X72d4
H E 0X737e
H 6 0X7432
H 2 0X74dc
H 4 0X75fe
H 7 0X76e4
H 1 0X7798
H 3 0X7824
H T 0X7914
B 5 0X7a04
B O 0X7ab8
B 9 0X7b62
B E 0X7c98
B 8 0X7d4c
B 2 0X7e28
B 6 0X7ec8
B 7 0X7fd6
B 4 0X8080
B 3 0X813e
B 1 0X81fc
B T 0X82ec
H 5 0X849a
H 9 0X854e
H O 0X8634
H 8 0X86f2
H E 0X8792
H 6 0X88be
H 2 0X897c
H 4 0X8a62
H 7 0X8b3e
H 1 0X8c1a
H 3 0X8c9c
H T 0X8d64
B 9 0X8e54
B 5 0X8f26
B O 0X900c
B E 0X90f2
B 8 0X91b0
B 2 0X9296
B 6 0X934a
B 7 0X943a
B 4 0X94bc
B 3 0X95a2
B 1 0X9692
B T 0X973c
H 9 0X98d6
H 5 0X991c
H E 0X9a66
H O 0X9b6a
H 2 0X9c32
H 8 0X9cf0
H 7 0X9e26
H 6 0X9ebc
H 3 0X9f8e
H 4 0Xa042
H 1 0Xa13c
H T 0Xa1f0
B 5 0Xa2ae
B E 0Xa394
B 9 0Xa43e
B 2 0Xa542
B O 0Xa5ec
B 7 0Xa696
B 8 0Xa75e
B 3 0Xa858
B 6 0Xa92a
B 1 0Xaa1a
B 4 0Xaae2
B T 0Xabbe
H E 0Xad62
H 5 0Xae0c
H 9 0Xaee8
H O 0Xafec
H 2 0Xb0a0
H 8 0Xb140
H 7 0Xb230
H 6 0Xb320
H 3 0Xb3b6
H 4 0Xb4b0
H 1 0Xb56e
H T 0Xb65e
B E 0Xb74e
B 9 0Xb7ee
B 5 0Xb8d4
B 2 0Xb9c4
B O 0Xba64
B 7 0Xbb22
B 8 0Xbbe0
B 3 0Xbcda
B 6 0Xbda2
B 1 0Xbe6a
B 4 0Xbf3c
B T 0Xc004
H 9 0Xc1c6
H E 0Xc2b6
H 5 0Xc356
H O 0Xc45a
H 2 0Xc518
H 8 0Xc5cc
H 7 0Xc694
H 6 0Xc784
H 3 0Xc838
H 4 0Xc91e
H 1 0Xca0e
H T 0Xcaa4
B 9 0Xcb94
B 5 0Xcc66
B E 0Xcd6a
B 2 0Xce28
B O 0Xcebe
B 7 0Xcf86
B 8 0Xd044
B 3 0Xd13e
B 6 0Xd1fc
B 1 0Xd300
B 4 0Xd3d2
B T 0Xd486
H 5 0Xd634
H 9 0Xd706
H 2 0Xd7ce
H E 0Xd8aa
H 7 0Xd97c
H O 0Xda4e
H 3 0Xdb16
H 8 0Xdbfc
H 1 0Xdd00
H 6 0Xdd6e
H 4 0Xde40
H T 0Xdf26
B 9 0Xe00c
B 5 0Xe0de
B 2 0Xe1ce
B 7 0Xe28c
B E 0Xe34a
B 3 0Xe426
B O 0Xe4da
B 1 0Xe5f2
B 8 0Xe692
B 4 0Xe732
B 6 0Xe7f0
B T 0Xe8e0
H 9 0Xeade
H 2 0Xeb9c
H 5 0Xec3c
H E 0Xed22
H 7 0Xee12
H O 0Xeef8
H 3 0Xef84
H 8 0Xf060
H 1 0Xf15a
H 6 0Xf204
H 4 0Xf2ae
H T 0Xf394
B 2 0Xf484
B 9 0Xf574
B 5 0Xf600
B 7 0Xf6e6
B E 0Xf844
B 3 0Xf88a
B O 0Xf966
B 1 0Xfa38
B 8 0Xfb00
B 4 0Xfbb4
B 6 0Xfca4
B T 0Xfd80
H 2 0Xff24
H 5 0Xffce
H 9 0X00be
H E 0X0190
H 7 0X0276
H O 0X0348
H 3 0X03ca
H 8 0X0492
H 1 0X0578
H 6 0X06a4
H 4 0X076c
H T 0X082a
B 5 0X08f2
B 2 0X09e2
B 9 0X0abe
B 7 0X0b68
B E 0X0cb2
B 3 0X0cf8
B O 0X0dd4
B 1 0X0e7e
B 8 0X0f50
B 4 0X1018
B 6 0X1112
B T 0X1202
H 2 0X1392
H 5 0X1464
H 7 0X155e
H 9 0X161c
H 3 0X1716
H E 0X178e
H 1 0X1860
H O 0X1964
H 4 0X1a2c
H 8 0X1ae0
H 6 0X1bbc
H T 0X1c8e
B 2 0X1d4c
B 7 0X1e32
B 5 0X1efa
B 3 0X201c
B 9 0X20b2
B 1 0X2170
B E 0X2210
B 4 0X231e
B O 0X23d2
B 6 0X24a4
B 8 0X259e
B T 0X265c
H 7 0X2846
H 2 0X2904
H 5 0X29a4
H 9 0X2a9e
H 3 0X2b48
H E 0X2c06
H 1 0X2cec
H O 0X2dd2
H 4 0X2e54
H 8 0X2f4e
H 6 0X302a
H T 0X3110
B 7 0X320a
B 5 0X32e6
B 2 0X33d6
B 3 0X348a
B 9 0X3548
B 1 0X3606
B E 0X3688
B 4 0X3750
B O 0X382c
B 6 0X393a
B 8 0X39f8
B T 0X3ad4
H 5 0X3cbe
H 7 0X3d9a
H 2 0X3e4e
H 9 0X3f0c
H 3 0X3ffc
H E 0X409c
H 1 0X4164
H O 0X425e
H 4 0X42d6
H 8 0X43c6
H 6 0X4498
H T 0X4588
B 5 0X4628
B 2 0X4722
B 7 0X47fe
B 3 0X490c
B 9 0X49c0
B 1 0X4a9c
B E 0X4b5a
B 4 0X4bfa
B O 0X4cc2
B 6 0X4d9e
B 8 0X4e5c
B T 0X4f4c
H 2 0X510e
H 5 0X5190
H 3 0X52a8
H 7 0X537a
H 1 0X549c
H 9 0X5528
H 4 0X55dc
H E 0X56a4
H 6 0X5780
H O 0X583e
H 8 0X58fc
H T 0X5a0a
B 5 0X5aaa
B 2 0X5b9a
B 3 0X5c62
B 1 0X5d5c
B 7 0X5e06
B 4 0X5f00
B 9 0X5fbe
B 6 0X605e
B E 0X6158
B 8 0X620c
B O 0X62de
B T 0X63ba
H 5 0X655e
H 3 0X661c
H 2 0X66e4
H 7 0X67c0
H 1 0X68ec
H 9 0X6982
H 4 0X6a5e
H E 0X6b1c
H 6 0X6bd0
H O 0X6cc0
H 8 0X6d60
H T 0X6e78
B 3 0X6f72
B 5 0X7026
B 2 0X70f8
B 1 0X71d4
B 7 0X726a
B 4 0X733c
B 9 0X740e
B 6 0X74d6
B E 0X75d0
B 8 0X7670
B O 0X7738
B T 0X781e
H 3 0X79f4
H 2 0X7ab2
H 5 0X7b5c
H 7 0X7c60
H 1 0X7d64
H 9 0X7de6
H 4 0X7ee0
H E 0X7f94
H 6 0X8048
H O 0X812e
H 8 0X81e2
H T 0X8304
B 2 0X83cc
B 3 0X848a
B 5 0X8534
B 1 0X8642
B 7 0X86e2
B 4 0X87be
B 9 0X8890
B 6 0X8930
B E 0X8a3e
B 8 0X8ade
B O 0X8bce
B T 0X8c96
H 3 0X8e44
H 2 0X8f2a
H 1 0X902e
H 5 0X90c4
H 4 0X91be
H 7 0X9272
H 6 0X9326
H 9 0X93da
H 8 0X94b6
H E 0X959c
H O 0X966e
H T 0X9754
B 3 0X981c
B 1 0X98e4
B 2 0X9998
B 4 0X9a7e
B 5 0X9b3c
B 6 0X9c2c
B 7 0X9cea
B 8 0X9d9e
B 9 0X9e7a
B O 0X9f4c
B E 0Xa014
B T 0Xa0f0
H 1 0Xa2f8
H 3 0Xa398
H 2 0Xa44c
H 5 0Xa50a
H 4 0Xa5f0
H 7 0Xa6c2
H 6 0Xa780
H 9 0Xa848
H 8 0Xa91a
H E 0Xa9ce
H O 0Xaad2
H T 0Xab90
B 1 0Xac76
B 2 0Xad3e
B 3 0Xae10
B 4 0Xaeb0
B 5 0Xaf78
B 6 0Xb054
B 7 0Xb126
B 8 0Xb1e4
B 9 0Xb2de
B O 0Xb3a6
B E 0Xb496
B T 0Xb540
H 1 0Xb73e
H 2 0Xb7c0
H 3 0Xb860
H 4 0Xb91e
H 5 0Xb9dc
H 6 0Xbaea
H 7 0Xbbd0
H 8 0Xbcac
H 9 0Xbd4c
H O 0Xbe46
H E 0Xbef0
H T 0Xbfae
B 1 0Xc0c6
B 2 0Xc17a
B 3 0Xc22e
B 4 0Xc2ce
B 5 0Xc382
B 6 0Xc47c
B 7 0Xc562
B 8 0Xc634
B 9 0Xc706
B O 0Xc7ec
B E 0Xc8d2
B T 0Xc972
H 1 0Xcb8e
H 2 0Xcc1a
H 3 0Xccba
H 4 0Xcd82
H 5 0Xce36
H 6 0Xcf1c
H 7 0Xd016
H 8 0Xd0ca
H 9 0Xd19c
H O 0Xd296
H E 0Xd336
H T 0Xd3f4
B 1 0Xd4f8
B 2 0Xd5b6
B 3 0Xd67e
B 4 0Xd728
B 5 0Xd804
B 6 0Xd908
B 7 0Xd9d0
B 8 0Xdac0
B 9 0Xdb7e
B O 0Xdc64
B E 0Xdd68
B T 0Xde1c
