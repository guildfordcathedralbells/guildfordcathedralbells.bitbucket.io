@echo off                                                           
  if exist C:\Octave\Octave-4.4.1\bin\octave.bat         goto doit                                
  echo .******************************************		     		
  echo .       HawkEar Cannot find GNU Octave			     		
  echo .													     	
  echo .       Please rerun the 							     	
  echo .													     	
  echo .             findoctave 							     	
  echo .													     	
  echo .       command to correct the problem			     		
  echo .******************************************		     		
  goto rexit												     	
:doit                                                      		
if not . == .%1 goto okr1                                  		
call C:\Octave\Octave-4.4.1\bin\octave.bat         -q --eval "addpath('%HKADDPATH%'); octver"	
goto rexit                                              			
:okr1                                                   			
call C:\Octave\Octave-4.4.1\bin\octave.bat         %1 %2 %3 %4 %5 %6    	       			
:rexit                                                  			
