==================================
User defined extensions to HawkEar
==================================

    You can now extend HawkEar to do something you want to do when issuing a
    HawkEar command, in addition of course to doing what the HawkEar command
    normally does.

    For example you might want to publish analyses automatically to a web 
    server. By extending the analyse command with your own scripts you
    can synchronise the Ringing Files\Analyses folder with a
    folder on the web server.


============
How it works
============

    - HawkEar will launch user defined scripts, if they exist, at the
      appropriate times

    - HawkEar will wait for the script complete before continuing with
      its own actions.

    - If a script crashes the HawkEar command will terminate, so use this
      facility at your own risk. Before contacting HawkEar support with a
      problem always delete your scripts and confirm the problem still 
      exists without it.

    - Script output will appear in the HawkEar window.

    - Your scripts will be provided with the same parameters as the HawkEar
      command.

    - You may use (but not change) the Environment variables listed in the
      HawkEar reference manual

    - Scripts must be located in %USERPROFILE%\Ringing Files\Scripts. 


===========================
Currently supported scripts:
===========================

    Currently two scripting points have been implemented:
    
    - In the analysis command prior to analysis: pre_analyse.cmd
    
    - In the analysis command after analysis: post_analyse.cmd



===============
Example scripts
===============

    -----------------------------------------------
    pre_analyse.cmd:
       @echo Hello from the pre-analyse script

    -----------------------------------------------
    post_analyse.cmd
       @echo Hello from the post-analyse script

    -----------------------------------------------
    post_analyse.cmd
       @echo off
       rem Refresh the web server
       robocopy %HKWEBANAL% \\192.168.123.45\share /s

    -----------------------------------------------


================
More Information
================

There is no more information. This is it! Good Luck!

