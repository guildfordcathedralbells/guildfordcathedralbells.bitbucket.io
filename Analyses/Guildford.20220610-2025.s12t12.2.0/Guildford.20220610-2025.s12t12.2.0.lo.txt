#. Lowndes: Version 2
#. SourceFilename: W:\Recordings\Guildford\Guildford.20220610-2025.s12t12.2.wav
#. Tower: Guildford Cathedral
#. RingingName: Touch 2
#. RingingDateTime: 20220610-2025
#. Rungon: 
#. Tenor: 12
#. NumBellsRinging: 12
#. Creator: HawkEar Team Edition V1 for Guildford Cathedral
#. CreatorVersion: 1.1.0.3
#. TranscribedFor: Guildford band
#. TranscriptionDateTime: 20220622-1614
#. FirstBlowMs: 49610
#. Touchdb: -19.1061
#. TraindB: -9.73, -8.5, -6.1, -3.11, -3.16, -3.1, -3.98, -4.12, -5.95, -5.43, -4.92, -3
#. TrainedOn: 20220102-1438
#. TrainDataRecordedOn: 20210813-1741
#. TrainParams: -ft:y -ws:0.05 -hs:0.01 -co:0.9 -sw:40 -ts:
#. GainParams: -fh:6000 -fl:200
#. OnsetParams: -mb:80 -mf:10 -ng:0.3 -oo:0
#. WriteParams: -hb:y
#. AnalParams: -au:y -xp:n -sg:n -hsg:-1
#. ExpString: 
#. Association: 
#. Calling: 
#. Compositionname: 
#. Method: 
#. Footnotes: 
#. Ringers: 
#. Stage: 
H 1 0Xc1ca
H 2 0Xc292
H 3 0Xc378
H 4 0Xc440
H 5 0Xc512
H 6 0Xc5e4
H 7 0Xc6c0
H 8 0Xc774
H 9 0Xc83c
H O 0Xc904
H E 0Xca58
H T 0Xcb7a
B 1 0Xcc24
B 2 0Xccd8
B 3 0Xcd96
B 4 0Xce54
B 5 0Xcf12
B 6 0Xcfee
B 7 0Xd0a2
B 8 0Xd174
B 9 0Xd25a
B O 0Xd340
B E 0Xd408
B T 0Xd50c
H 1 0Xd6ec
H 2 0Xd7aa
H 3 0Xd84a
H 4 0Xd908
H 5 0Xd9c6
H 6 0Xda7a
H 7 0Xdb60
H 8 0Xdbf6
H 9 0Xdcd2
H O 0Xddc2
H E 0Xde6c
H T 0Xdf66
B 1 0Xe06a
B 2 0Xe132
B 3 0Xe1d2
B 4 0Xe29a
B 5 0Xe344
B 6 0Xe420
B 7 0Xe4e8
B 8 0Xe5a6
B 9 0Xe682
B O 0Xe736
B E 0Xe826
B T 0Xe8ee
H 1 0Xeab0
H 2 0Xeb6e
H 3 0Xec2c
H 4 0Xed26
H 5 0Xedd0
H 6 0Xeeac
H 7 0Xef6a
H 8 0Xf03c
H 9 0Xf10e
H O 0Xf1ae
H E 0Xf276
H T 0Xf352
B 1 0Xf41a
B 2 0Xf4d8
B 3 0Xf596
B 4 0Xf668
B 5 0Xf712
B 6 0Xf7ee
B 7 0Xf8b6
B 8 0Xf988
B 9 0Xfa82
B O 0Xfb22
B E 0Xfc08
B T 0Xfce4
H 1 0Xfeb0
H 2 0Xff50
H 3 0Xfffa
H 4 0X00ae
H 5 0X016c
H 6 0X0234
H 7 0X0310
H 8 0X03ce
H 9 0X04aa
H O 0X05a4
H E 0X0630
H T 0X070c
B 1 0X07fc
B 2 0X08ba
B 3 0X0950
B 4 0X0a22
B 5 0X0ad6
B 6 0X0bbc
B 7 0X0c84
B 8 0X0d42
B 9 0X0e28
B O 0X0efa
B E 0X0fcc
B T 0X108a
H 1 0X1288
H 2 0X131e
H 3 0X13c8
H 4 0X1468
H 5 0X153a
H 6 0X15f8
H 7 0X16e8
H 8 0X17b0
H 9 0X1864
H O 0X1936
H E 0X19d6
H T 0X1ad0
B 1 0X1bc0
B 2 0X1c60
B 3 0X1d28
B 4 0X1dfa
B 5 0X1e7c
B 6 0X1f80
B 7 0X2052
B 8 0X2124
B 9 0X21f6
B O 0X2296
B E 0X2390
B T 0X2430
H 1 0X261a
H 2 0X26a6
H 3 0X275a
H 4 0X2836
H 5 0X28d6
H 6 0X29a8
H 7 0X2ac0
H 8 0X2b88
H 9 0X2c5a
H O 0X2d18
H E 0X2dc2
H T 0X2e80
B 1 0X2f66
B 2 0X301a
B 3 0X30c4
B 4 0X318c
B 5 0X3236
B 6 0X3330
B 7 0X342a
B 8 0X34d4
B 9 0X35c4
B O 0X3664
B E 0X3722
B T 0X3812
H 1 0X39f2
H 2 0X3a88
H 3 0X3b28
H 4 0X3c18
H 5 0X3ca4
H 6 0X3d76
H 7 0X3e8e
H 8 0X3f38
H 9 0X3fe2
H O 0X40be
H E 0X4172
H T 0X4262
B 1 0X4316
B 2 0X43d4
B 3 0X447e
B 4 0X4550
B 5 0X4604
B 6 0X46f4
B 7 0X47ee
B 8 0X48a2
B 9 0X4960
B O 0X4a1e
B E 0X4ae6
B T 0X4bc2
H 1 0X4d84
H 2 0X4e10
H 3 0X4eba
H 4 0X4f82
H 5 0X5040
H 6 0X513a
H 7 0X522a
H 8 0X52d4
H 9 0X53a6
H O 0X5464
H E 0X5504
H T 0X55d6
B 1 0X56f8
B 2 0X5784
B 3 0X582e
B 4 0X591e
B 5 0X59aa
B 6 0X5a72
B 7 0X5b8a
B 8 0X5c3e
B 9 0X5d10
B O 0X5dc4
B E 0X5ea0
B T 0X5f4a
H 1 0X6152
H 2 0X61e8
H 3 0X6292
H 4 0X636e
H 5 0X6404
H 6 0X64d6
H 7 0X65e4
H 8 0X667a
H 9 0X6738
H O 0X6828
H E 0X68be
H T 0X69a4
B 1 0X6ab2
B 2 0X6b48
B 3 0X6bfc
B 4 0X6cce
B 5 0X6d8c
B 6 0X6e54
B 7 0X6f26
B 8 0X6ff8
B 9 0X70ac
B O 0X716a
B E 0X7264
B T 0X7322
H 1 0X7520
H 2 0X75a2
H 3 0X766a
H 4 0X773c
H 5 0X77dc
H 6 0X78ae
H 7 0X79c6
H 8 0X7a84
H 9 0X7b1a
H O 0X7be2
H E 0X7c64
H T 0X7d40
B 1 0X7e58
B 2 0X7ef8
B 3 0X7fca
B 4 0X80ce
B 5 0X8164
B 6 0X8236
B 7 0X8326
B 8 0X83d0
B 9 0X8484
B O 0X8556
B E 0X861e
B T 0X86c8
H 1 0X88c6
H 2 0X8952
H 3 0X8a10
H 4 0X8af6
H 5 0X8b82
H 6 0X8c68
H 7 0X8d6c
H 8 0X8e34
H 9 0X8ed4
H O 0X8fba
H E 0X9050
H T 0X9122
B 1 0X9226
B 2 0X92da
B 3 0X9398
B 4 0X9442
B 5 0X94e2
B 6 0X95d2
B 7 0X96d6
B 8 0X9776
B 9 0X9820
B O 0X991a
B E 0X99ec
B T 0X9aa0
H 1 0X9c76
H 2 0X9d02
H 3 0X9dca
H 4 0X9ec4
H 5 0X9f46
H 6 0Xa02c
H 7 0Xa130
H 8 0Xa1d0
H 9 0Xa27a
H O 0Xa37e
H E 0Xa400
H T 0Xa4dc
B 1 0Xa5ae
B 2 0Xa66c
B 3 0Xa72a
B 4 0Xa810
B 5 0Xa8c4
B 6 0Xa9a0
B 7 0Xaaa4
B 8 0Xab62
B 9 0Xac2a
B O 0Xacde
B E 0Xadb0
B T 0Xae6e
H 2 0Xb01c
H 1 0Xb10c
H 3 0Xb1a2
H 5 0Xb26a
H 4 0Xb346
H 7 0Xb418
H 6 0Xb4d6
H 9 0Xb58a
H 8 0Xb666
H E 0Xb724
H O 0Xb81e
H T 0Xb8d2
B 2 0Xb9c2
B 3 0Xba6c
B 1 0Xbb16
B 4 0Xbbde
B 5 0Xbc9c
B 6 0Xbd96
B 7 0Xbe68
B 8 0Xbf3a
B 9 0Xbff8
B O 0Xc0c0
B E 0Xc174
B T 0Xc25a
H 3 0Xc430
H 2 0Xc4da
H 4 0Xc5c0
H 1 0Xc67e
H 6 0Xc714
H 5 0Xc7c8
H 8 0Xc8ae
H 7 0Xc962
H 9 0Xca52
H E 0Xcae8
H O 0Xcbe2
H T 0Xcc8c
B 2 0Xcda4
B 3 0Xce44
B 4 0Xcf16
B 6 0Xcfca
B 1 0Xd09c
B 8 0Xd150
B 5 0Xd218
B 9 0Xd2f4
B 7 0Xd3a8
B O 0Xd466
B E 0Xd538
B T 0Xd60a
H 2 0Xd7e0
H 4 0Xd89e
H 3 0Xd970
H 1 0Xda4c
H 6 0Xdb00
H 5 0Xdbbe
H 8 0Xdc86
H 7 0Xdd4e
H 9 0Xde16
H E 0Xdea2
H O 0Xdf9c
H T 0Xe046
B 4 0Xe172
B 2 0Xe208
B 3 0Xe2d0
B 6 0Xe3a2
B 1 0Xe456
B 8 0Xe53c
B 5 0Xe5fa
B 9 0Xe6cc
B 7 0Xe794
B O 0Xe820
B E 0Xe8d4
B T 0Xe9c4
H 4 0Xeba4
H 3 0Xec80
H 2 0Xed02
H 1 0Xee10
H 6 0Xeea6
H 5 0Xef8c
H 8 0Xf054
H 7 0Xf14e
H 9 0Xf1d0
H E 0Xf270
H O 0Xf338
H T 0Xf428
B 3 0Xf52c
B 4 0Xf608
B 2 0Xf6bc
B 6 0Xf784
B 1 0Xf82e
B 8 0Xf8f6
B 5 0Xf9f0
B 9 0Xfa7c
B 7 0Xfb76
B O 0Xfc02
B E 0Xfcd4
B T 0Xfdb0
H 4 0Xff86
H 3 0X004e
H 6 0X013e
H 2 0X0224
H 8 0X02c4
H 1 0X038c
H 9 0X0422
H 5 0X04ea
H O 0X059e
H 7 0X0666
H E 0X06f2
H T 0X07e2
B 4 0X0918
B 6 0X09e0
B 3 0X0ac6
B 8 0X0b7a
B 2 0X0c42
B 9 0X0cf6
B 1 0X0d8c
B O 0X0e86
B 5 0X0f1c
B E 0X0fb2
B 7 0X10ac
B T 0X1174
H 6 0X1354
H 4 0X1458
H 3 0X152a
H 2 0X15fc
H 8 0X169c
H 1 0X1778
H 9 0X1836
H 5 0X1912
H O 0X19b2
H 7 0X1a52
H E 0X1ade
H T 0X1bc4
B 6 0X1cbe
B 3 0X1dae
B 4 0X1e8a
B 8 0X1f48
B 2 0X2024
B 9 0X2114
B 1 0X2196
B O 0X22ae
B 5 0X233a
B E 0X239e
B 7 0X24b6
B T 0X2574
H 3 0X2768
H 6 0X2826
H 4 0X28e4
H 2 0X29c0
H 8 0X2a7e
H 1 0X2b6e
H 9 0X2c0e
H 5 0X2d08
H O 0X2da8
H 7 0X2e48
H E 0X2eb6
H T 0X2fba
B 3 0X30f0
B 4 0X31c2
B 6 0X3258
B 8 0X3334
B 2 0X33de
B 9 0X34ce
B 1 0X3578
B O 0X3672
B 5 0X3726
B E 0X379e
B 7 0X38a2
B T 0X3960
H 4 0X3b40
H 3 0X3c08
H 8 0X3ce4
H 6 0X3dca
H 9 0X3e4c
H 2 0X3f00
H O 0X3ff0
H 1 0X40cc
H 5 0X416c
H 7 0X422a
H E 0X42a2
H T 0X4388
B 3 0X44c8
B 4 0X459a
B 8 0X464e
B 9 0X470c
B 6 0X47ca
B O 0X48ba
B 2 0X4964
B 5 0X4a4a
B 1 0X4afe
B E 0X4b94
B 7 0X4c70
B T 0X4d42
H 3 0X4f18
H 8 0X4fea
H 4 0X50b2
H 6 0X517a
H 9 0X5238
H 2 0X5300
H O 0X53dc
H 1 0X54ae
H 5 0X5558
H 7 0X562a
H E 0X56ac
H T 0X5788
B 8 0X58d2
B 3 0X597c
B 4 0X5a3a
B 9 0X5ada
B 6 0X5bde
B O 0X5cc4
B 2 0X5d46
B 5 0X5e22
B 1 0X5f12
B E 0X5f94
B 7 0X602a
B T 0X6142
H 8 0X6322
H 4 0X63d6
H 3 0X64b2
H 6 0X658e
H 9 0X6638
H 2 0X6714
H O 0X67dc
H 1 0X68cc
H 5 0X6930
H 7 0X6a02
H E 0X6a7a
H T 0X6bb0
B 4 0X6ca0
B 8 0X6d40
B 3 0X6e4e
B 9 0X6f02
B 6 0X6ff2
B O 0X70ba
B 2 0X7150
B 5 0X7204
B 1 0X72e0
B E 0X7362
B 7 0X742a
B T 0X7542
H 8 0X7722
H 4 0X77b8
H 9 0X7894
H 3 0X7952
H O 0X7a42
H 6 0X7aec
H 5 0X7bbe
H 2 0X7c9a
H E 0X7d3a
H 1 0X7e20
H 7 0X7eac
H T 0X7f92
B 8 0X8050
B 9 0X8172
B 4 0X821c
B O 0X832a
B 3 0X83ca
B 5 0X8488
B 6 0X8550
B E 0X8604
B 2 0X86e0
B 7 0X8780
B 1 0X8852
B T 0X892e
H 9 0X8b22
H 8 0X8bc2
H 4 0X8cb2
H 3 0X8d84
H O 0X8e38
H 6 0X8ef6
H 5 0X8fc8
H 2 0X90a4
H E 0X913a
H 1 0X91e4
H 7 0X92ac
H T 0X9360
B 9 0X9464
B 4 0X9586
B 8 0X963a
B O 0X970c
B 3 0X97de
B 5 0X98a6
B 6 0X9964
B E 0X9a0e
B 2 0X9ac2
B 7 0X9bee
B 1 0X9c7a
B T 0X9d24
H 4 0X9ef0
H 9 0X9fae
H 8 0Xa08a
H 3 0Xa17a
H O 0Xa238
H 6 0Xa300
H 5 0Xa3dc
H 2 0Xa4b8
H E 0Xa51c
H 1 0Xa602
H 7 0Xa6e8
H T 0Xa79c
B 4 0Xa896
B 8 0Xa968
B 9 0Xa9fe
B O 0Xab0c
B 3 0Xabc0
B 5 0Xac7e
B 6 0Xad82
B E 0Xadf0
B 2 0Xaec2
B 7 0Xafee
B 1 0Xb084
B T 0Xb138
H 8 0Xb32c
H 4 0Xb3cc
H O 0Xb4c6
H 9 0Xb570
H 5 0Xb656
H 3 0Xb700
H E 0Xb78c
H 6 0Xb886
H 7 0Xb9b2
H 2 0Xba02
H 1 0Xbb06
H T 0Xbba6
B 4 0Xbca0
B 8 0Xbd5e
B O 0Xbe30
B 5 0Xbf7a
B 9 0Xbfde
B E 0Xc092
B 3 0Xc182
B 7 0Xc24a
B 6 0Xc312
B 1 0Xc39e
B 2 0Xc448
B T 0Xc542
H 4 0Xc718
H O 0Xc7c2
H 8 0Xc876
H 9 0Xc95c
H 5 0Xca4c
H 3 0Xcb28
H E 0Xcba0
H 6 0Xcc86
H 7 0Xcd8a
H 2 0Xce16
H 1 0Xcf06
H T 0Xcf9c
B O 0Xd096
B 4 0Xd17c
B 8 0Xd226
B 5 0Xd302
B 9 0Xd3de
B E 0Xd4a6
B 3 0Xd56e
B 7 0Xd64a
B 6 0Xd6fe
B 1 0Xd78a
B 2 0Xd85c
B T 0Xd924
H O 0Xdb36
H 8 0Xdc12
H 4 0Xdcc6
H 9 0Xdd84
H 5 0Xde42
H 3 0Xdf14
H E 0Xdf8c
H 6 0Xe0ae
H 7 0Xe18a
H 2 0Xe248
H 1 0Xe310
H T 0Xe37e
B 8 0Xe4d2
B O 0Xe586
B 4 0Xe676
B 5 0Xe70c
B 9 0Xe7de
B E 0Xe8b0
B 3 0Xe96e
B 7 0Xea4a
B 6 0Xeb3a
B 1 0Xebd0
B 2 0Xec84
B T 0Xed38
H O 0Xef22
H 8 0Xeffe
H 5 0Xf0b2
H 4 0Xf198
H E 0Xf24c
H 9 0Xf314
H 7 0Xf436
H 3 0Xf4cc
H 1 0Xf5c6
H 6 0Xf684
H 2 0Xf710
H T 0Xf7c4
B O 0Xf8a0
B 5 0Xf97c
B 8 0Xfa30
B E 0Xfb3e
B 4 0Xfc06
B 7 0Xfc9c
B 9 0Xfd8c
B 1 0Xfefe
B 3 0Xff30
B 2 0Xfff8
B 6 0X00d4
B T 0X017e
H 5 0X0336
H O 0X03fe
H 8 0X04e4
H 4 0X0598
H E 0X0638
H 9 0X073c
H 7 0X0818
H 3 0X08f4
H 1 0X0994
H 6 0X0a66
H 2 0X0b1a
H T 0X0bce
B 5 0X0cd2
B 8 0X0db8
B O 0X0e6c
B E 0X0f52
B 4 0X1010
B 7 0X10ec
B 9 0X1182
B 1 0X125e
B 3 0X1308
B 2 0X1416
B 6 0X14ac
B T 0X1588
H 8 0X1786
H 5 0X183a
H O 0X18e4
H 4 0X19e8
H E 0X1a42
H 9 0X1b3c
H 7 0X1c0e
H 3 0X1ce0
H 1 0X1da8
H 6 0X1e5c
H 2 0X1efc
H T 0X1fec
B 8 0X20d2
B O 0X21cc
B 5 0X229e
B E 0X2320
B 4 0X244c
B 7 0X250a
B 9 0X25aa
B 1 0X26a4
B 3 0X274e
B 2 0X280c
B 6 0X28e8
B T 0X297e
H O 0X2b68
H 8 0X2c1c
H E 0X2cda
H 5 0X2dca
H 7 0X2eb0
H 4 0X2f64
H 1 0X305e
H 9 0X3130
H 2 0X320c
H 3 0X32b6
H 6 0X336a
H T 0X33f6
B 8 0X34c8
B O 0X35cc
B E 0X36a8
B 7 0X3748
B 5 0X382e
B 1 0X3928
B 4 0X39dc
B 2 0X3acc
B 9 0X3ba8
B 6 0X3c2a
B 3 0X3cfc
B T 0X3dba
H 8 0X3f9a
H E 0X401c
H O 0X40f8
H 5 0X41de
H 7 0X42c4
H 4 0X438c
H 1 0X4454
H 9 0X4526
H 2 0X45d0
H 3 0X4698
H 6 0X477e
H T 0X4828
B E 0X4904
B 8 0X49cc
B O 0X4aa8
B 7 0X4b84
B 5 0X4c56
B 1 0X4d0a
B 4 0X4e0e
B 2 0X4ecc
B 9 0X4f80
B 6 0X5048
B 3 0X5110
B T 0X51c4
H E 0X534a
H O 0X544e
H 8 0X5516
H 5 0X55fc
H 7 0X570a
H 4 0X57be
H 1 0X585e
H 9 0X5926
H 2 0X59bc
H 3 0X5a98
H 6 0X5b7e
H T 0X5c3c
B O 0X5d22
B E 0X5dea
B 8 0X5eb2
B 7 0X5fb6
B 5 0X6074
B 1 0X6146
B 4 0X6222
B 2 0X62e0
B 9 0X6394
B 6 0X645c
B 3 0X652e
B T 0X65ce
H E 0X6754
H O 0X686c
H 7 0X6948
H 8 0X6a10
H 1 0X6b5a
H 5 0X6bbe
H 2 0X6c90
H 4 0X6d6c
H 6 0X6e0c
H 9 0X6ede
H 3 0X6f7e
H T 0X7050
B E 0X7136
B 7 0X7230
B O 0X72b2
B 1 0X73e8
B 8 0X7456
B 2 0X7582
B 5 0X7604
B 6 0X76cc
B 4 0X77c6
B 3 0X787a
B 9 0X7942
B T 0X79ec
H 7 0X7bd6
H E 0X7c4e
H O 0X7d48
H 8 0X7e42
H 1 0X7f0a
H 5 0X7fbe
H 2 0X8068
H 4 0X8162
H 6 0X8216
H 9 0X82de
H 3 0X83c4
H T 0X8482
B 7 0X854a
B O 0X8612
B E 0X870c
B 1 0X87de
B 8 0X8892
B 2 0X8964
B 5 0X89fa
B 6 0X8ae0
B 4 0X8bda
B 3 0X8ca2
B 9 0X8d56
B T 0X8e1e
H O 0X8fea
H 7 0X90d0
H E 0X9170
H 8 0X9274
H 1 0X935a
H 5 0X93dc
H 2 0X9490
H 4 0X958a
H 6 0X965c
H 9 0X971a
H 3 0X97ce
H T 0X98a0
B O 0X9954
B E 0X9a30
B 7 0X9b16
B 1 0X9bfc
B 8 0X9cc4
B 2 0X9d82
B 5 0X9e22
B 6 0X9f1c
B 4 0Xa00c
B 3 0Xa0ca
B 9 0Xa17e
B T 0Xa232
H E 0Xa3ea
H O 0Xa4da
H 1 0Xa5a2
H 7 0Xa674
H 2 0Xa73c
H 8 0Xa7f0
H 6 0Xa8ea
H 5 0Xa99e
H 3 0Xaa66
H 4 0Xab38
H 9 0Xac00
H T 0Xacc8
B O 0Xad7c
B E 0Xae8a
B 1 0Xaf52
B 2 0Xb02e
B 7 0Xb0d8
B 6 0Xb1a0
B 8 0Xb24a
B 3 0Xb358
B 5 0Xb3ee
B 9 0Xb4d4
B 4 0Xb5ce
B T 0Xb678
H O 0Xb830
H 1 0Xb934
H E 0Xb998
H 7 0Xba88
H 2 0Xbb46
H 8 0Xbc0e
H 6 0Xbcd6
H 5 0Xbdbc
H 3 0Xbe84
H 4 0Xbfb0
H 9 0Xc03c
H T 0Xc104
B 1 0Xc1d6
B O 0Xc280
B E 0Xc366
B 2 0Xc438
B 7 0Xc4ec
B 6 0Xc596
B 8 0Xc67c
B 3 0Xc776
B 5 0Xc820
B 9 0Xc910
B 4 0Xc9d8
B T 0Xcaaa
H 1 0Xcc44
H E 0Xccee
H O 0Xcdfc
H 7 0Xcec4
H 2 0Xcf5a
H 8 0Xd054
H 6 0Xd108
H 5 0Xd202
H 3 0Xd2d4
H 4 0Xd3a6
H 9 0Xd446
H T 0Xd50e
B E 0Xd5fe
B 1 0Xd6b2
B O 0Xd78e
B 2 0Xd874
B 7 0Xd91e
B 6 0Xd9e6
B 8 0Xdaae
B 3 0Xdba8
B 5 0Xdc66
B 9 0Xdd1a
B 4 0Xde14
B T 0Xdebe
H 1 0Xe076
H E 0Xe13e
H 2 0Xe210
H O 0Xe2e2
H 6 0Xe3be
H 7 0Xe468
H 3 0Xe544
H 8 0Xe62a
H 9 0Xe6ca
H 5 0Xe7a6
H 4 0Xe8a0
H T 0Xe922
B 1 0Xe9ea
B 2 0Xeaf8
B E 0Xeb70
B 6 0Xec7e
B O 0Xed46
B 3 0Xee36
B 7 0Xeef4
B 9 0Xef9e
B 8 0Xf066
B 4 0Xf14c
B 5 0Xf214
B T 0Xf2c8
H 2 0Xf4c6
H 1 0Xf584
H E 0Xf610
H O 0Xf6f6
H 6 0Xf7b4
H 7 0Xf8a4
H 3 0Xf962
H 8 0Xfa20
H 9 0Xfaf2
H 5 0Xfbc4
H 4 0Xfcaa
H T 0Xfd68
B 2 0Xfe3a
B E 0Xff02
B 1 0Xffde
B 6 0X00a6
B O 0X016e
B 3 0X0236
B 7 0X0326
B 9 0X03da
B 8 0X048e
B 4 0X057e
B 5 0X0632
B T 0X06fa
H E 0X08c6
H 2 0X098e
H 1 0X0aa6
H O 0X0b46
H 6 0X0bf0
H 7 0X0cf4
H 3 0X0d9e
H 8 0X0e52
H 9 0X0f38
H 5 0X0fd8
H 4 0X10dc
H T 0X1190
B E 0X1294
B 1 0X133e
B 2 0X1410
B 6 0X14c4
B O 0X1596
B 3 0X1672
B 7 0X1762
B 9 0X1816
B 8 0X18d4
B 4 0X19b0
B 5 0X1a78
B T 0X1b36
H 1 0X1cda
H E 0X1dac
H 6 0X1e74
H 2 0X1f82
H 3 0X2040
H O 0X20f4
H 9 0X21da
H 7 0X2284
H 4 0X2360
H 8 0X2414
H 5 0X24be
H T 0X25d6
B E 0X26d0
B 1 0X275c
B 6 0X2824
B 3 0X290a
B 2 0X29be
B 9 0X2aa4
B O 0X2b58
B 4 0X2c34
B 7 0X2cf2
B 5 0X2da6
B 8 0X2e6e
B T 0X2f5e
H E 0X313e
H 6 0X3210
H 1 0X32ce
H 2 0X3396
H 3 0X344a
H O 0X351c
H 9 0X35a8
H 7 0X36ca
H 4 0X374c
H 8 0X383c
H 5 0X38fa
H T 0X39fe
B 6 0X3ad0
B E 0X3bde
B 1 0X3c7e
B 3 0X3d46
B 2 0X3de6
B 9 0X3eae
B O 0X3f4e
B 4 0X4034
B 7 0X411a
B 5 0X41e2
B 8 0X42a0
B T 0X4386
H 6 0X44c6
H 1 0X462e
H E 0X46ce
H 2 0X47d2
H 3 0X4890
H O 0X4944
H 9 0X49f8
H 7 0X4afc
H 4 0X4bf6
H 8 0X4c82
H 5 0X4cfa
H T 0X4e12
B 1 0X4f0c
B 6 0X4fc0
B E 0X5074
B 3 0X515a
B 2 0X5218
B 9 0X52d6
B O 0X53bc
B 4 0X54ca
B 7 0X5574
B 5 0X560a
B 8 0X56dc
B T 0X57ae
H 6 0X5984
H 1 0X5a60
H 3 0X5b0a
H E 0X5bbe
H 9 0X5cb8
H 2 0X5d80
H 4 0X5e48
H O 0X5efc
H 5 0X5fba
H 7 0X60d2
H 8 0X619a
H T 0X6244
B 6 0X630c
B 3 0X63de
B 1 0X64ce
B 9 0X658c
B E 0X665e
B 4 0X671c
B 2 0X67c6
B 5 0X6898
B O 0X6960
B 8 0X6a82
B 7 0X6b0e
B T 0X6be0
H 3 0X6dd4
H 6 0X6e88
H 1 0X6f64
H E 0X6ffa
H 9 0X70fe
H 2 0X71a8
H 4 0X72a2
H O 0X7324
H 5 0X73e2
H 7 0X74d2
H 8 0X75ae
H T 0X7680
B 3 0X775c
B 1 0X782e
B 6 0X78ba
B 9 0X79b4
B E 0X7a72
B 4 0X7b6c
B 2 0X7be4
B 5 0X7cde
B O 0X7d9c
B 8 0X7e96
B 7 0X7f5e
B T 0X8012
H 1 0X81d4
H 3 0X8292
H 6 0X835a
H E 0X8418
H 9 0X84f4
H 2 0X85d0
H 4 0X86c0
H O 0X8792
H 5 0X8828
H 7 0X892c
H T 0X89fe
H 8 0X8a08
B 1 0X8b5c
B 6 0X8c38
B 3 0X8d1e
B 9 0X8dd2
B E 0X8eae
B 4 0X8f8a
B 2 0X902a
B 5 0X9110
B O 0X91d8
B 8 0X92f0
B 7 0X937c
B T 0X9444
H 6 0X95fc
H 1 0X9692
H 9 0X97b4
H 3 0X9854
H 4 0X991c
H E 0X99b2
H 5 0X9a8e
H 2 0X9b92
H 8 0X9c64
H O 0X9d18
H 7 0X9e12
H T 0X9eb2
B 1 0X9f3e
B 6 0Xa060
B 9 0Xa132
B 4 0Xa204
B 3 0Xa2cc
B 5 0Xa38a
B E 0Xa434
B 8 0Xa542
B 2 0Xa600
B 7 0Xa704
B O 0Xa786
B T 0Xa858
H 1 0Xa9f2
H 9 0Xaaba
H 6 0Xab82
H 3 0Xac5e
H 4 0Xad4e
H E 0Xae0c
H 5 0Xaec0
H 2 0Xafb0
H 8 0Xb06e
H O 0Xb14a
H 7 0Xb212
H T 0Xb2d0
B 9 0Xb3c0
B 1 0Xb460
B 6 0Xb550
B 4 0Xb636
B 3 0Xb6c2
B 5 0Xb7b2
B E 0Xb870
B 8 0Xb94c
B 2 0Xba14
B 7 0Xbadc
B O 0Xbba4
B T 0Xbc62
H 9 0Xbe9c
H 6 0Xbf14
H 1 0Xbff0
H 3 0Xc07c
H 4 0Xc180
H E 0Xc20c
H 5 0Xc2e8
H 2 0Xc3f6
H 8 0Xc4c8
H O 0Xc586
H 7 0Xc64e
H T 0Xc6ee
B 6 0Xc7de
B 9 0Xc8c4
B 1 0Xc982
B 4 0Xca54
B 3 0Xcb12
B 5 0Xcbd0
B E 0Xcc84
B 8 0Xcd9c
B 2 0Xce5a
B 7 0Xcf4a
B O 0Xcfe0
B T 0Xd0a8
H 9 0Xd274
H 6 0Xd350
H 4 0Xd40e
H 1 0Xd4ea
H 5 0Xd58a
H 3 0Xd63e
H 8 0Xd742
H E 0Xd80a
H 2 0Xd8dc
H O 0Xd9cc
H 7 0Xdaa8
H T 0Xdb20
B 9 0Xdc24
B 4 0Xdcec
B 6 0Xdd96
B 5 0Xde90
B 1 0Xdf58
B 8 0Xdff8
B 3 0Xe11a
B 2 0Xe1b0
B E 0Xe246
B 7 0Xe30e
B O 0Xe41c
B T 0Xe4ee
H 4 0Xe69c
H 9 0Xe764
H 6 0Xe82c
H 1 0Xe91c
H 5 0Xe9b2
H 3 0Xeaa2
H 8 0Xeb6a
H E 0Xec14
H 2 0Xecaa
H O 0Xeda4
H 7 0Xee80
H T 0Xef5c
B 4 0Xf006
B 6 0Xf114
B 9 0Xf1f0
B 5 0Xf2c2
B 1 0Xf36c
B 8 0Xf40c
B 3 0Xf524
B 2 0Xf5ba
B E 0Xf65a
B 7 0Xf736
B O 0Xf81c
B T 0Xf8f8
H 6 0Xfad8
H 4 0Xfba0
H 9 0Xfc40
H 1 0Xfd12
H 5 0Xfda8
H 3 0Xfe8e
H 8 0Xff60
H E 0X0082
H 2 0X00d2
H O 0X01d6
H 7 0X0294
H T 0X0352
B 6 0X0438
B 9 0X051e
B 4 0X05e6
B 5 0X06a4
B 1 0X076c
B 8 0X085c
B 3 0X0906
B 2 0X09e2
B E 0X0a96
B 7 0X0b4a
B O 0X0c4e
B T 0X0d02
H 9 0X0ec4
H 6 0X0f8c
H 5 0X1068
H 4 0X1144
H 8 0X1202
H 1 0X1306
H 2 0X137e
H 3 0X1446
H 7 0X1536
H E 0X15cc
H O 0X16bc
H T 0X1798
B 6 0X1874
B 9 0X1950
B 5 0X1a0e
B 8 0X1ac2
B 4 0X1b94
B 2 0X1c66
B 1 0X1d24
B 7 0X1dec
B 3 0X1ebe
B O 0X1f68
B E 0X204e
B T 0X2134
H 6 0X22f6
H 5 0X23c8
H 9 0X2490
H 4 0X256c
H 8 0X2620
H 1 0X26de
H 2 0X2774
H 3 0X285a
H 7 0X2940
H E 0X29fe
H O 0X2b0c
H T 0X2ba2
B 5 0X2c92
B 6 0X2d5a
B 9 0X2e40
B 8 0X2f12
B 4 0X2fee
B 2 0X308e
B 1 0X314c
B 7 0X3228
B 3 0X32b4
B O 0X3386
B E 0X346c
B T 0X352a
H 5 0X370a
H 9 0X37e6
H 6 0X387c
H 4 0X3994
H 8 0X3a5c
H 1 0X3afc
H 2 0X3bb0
H 3 0X3c8c
H 7 0X3d86
H E 0X3e30
H O 0X3ef8
H T 0X3fc0
B 9 0X40a6
B 5 0X4178
B 6 0X422c
B 8 0X431c
B 4 0X4434
B 2 0X44c0
B 1 0X459c
B 7 0X4650
B 3 0X46fa
B O 0X47b8
B E 0X48a8
B T 0X4970
H 5 0X4b14
H 9 0X4bf0
H 8 0X4ccc
H 6 0X4d9e
H 2 0X4ea2
H 4 0X4f74
H 7 0X5082
H 1 0X512c
H O 0X51b8
H 3 0X524e
H E 0X52e4
H T 0X53d4
B 5 0X54d8
B 8 0X5596
B 9 0X565e
B 2 0X578a
B 6 0X57f8
B 7 0X58fc
B 4 0X59ce
B O 0X5a8c
B 1 0X5b90
B E 0X5bea
B 3 0X5ce4
B T 0X5d84
H 8 0X5f8c
H 5 0X6022
H 9 0X60ea
H 6 0X61d0
H 2 0X62ac
H 4 0X6392
H 7 0X646e
H 1 0X654a
H O 0X65e0
H 3 0X66da
H E 0X6734
H T 0X6810
B 8 0X6914
B 9 0X6a04
B 5 0X6a86
B 2 0X6b9e
B 6 0X6c20
B 7 0X6d38
B 4 0X6df6
B O 0X6ebe
B 1 0X6f86
B E 0X7044
B 3 0X7116
B T 0X71ca
H 9 0X73d2
H 8 0X7486
H 5 0X7512
H 6 0X762a
H 2 0X76de
H 4 0X77ba
H 7 0X78aa
H 1 0X797c
H O 0X79fe
H 3 0X7ad0
H E 0X7b5c
H T 0X7c38
B 9 0X7d50
B 5 0X7e2c
B 8 0X7eb8
B 2 0X7fd0
B 6 0X8070
B 7 0X816a
B 4 0X821e
B O 0X82e6
B 1 0X83c2
B E 0X843a
B 3 0X8534
B T 0X85de
H 5 0X87be
H 9 0X8890
H 2 0X8958
H 8 0X8a34
H 7 0X8b42
H 6 0X8be2
H O 0X8cbe
H 4 0X8d72
H 1 0X8e62
H 3 0X8ef8
H E 0X8f98
H T 0X904c
B 9 0X9178
B 5 0X9254
B 2 0X9344
B 7 0X9416
B 8 0X94a2
B O 0X959c
B 6 0X9628
B 1 0X970e
B 4 0X97ea
B E 0X988a
B 3 0X9966
B T 0X9a10
H 9 0X9bdc
H 2 0X9cc2
H 5 0X9d8a
H 8 0X9e8e
H 7 0X9f6a
H 6 0Xa028
H O 0Xa0f0
H 4 0Xa19a
H 1 0Xa276
H 3 0Xa316
H E 0Xa3a2
H T 0Xa46a
B 2 0Xa58c
B 9 0Xa65e
B 5 0Xa708
B 7 0Xa802
B 8 0Xa8c0
B O 0Xa9ba
B 6 0Xaa8c
B 1 0Xab5e
B 4 0Xac4e
B E 0Xacda
B 3 0Xada2
B T 0Xae38
H 2 0Xb00e
H 5 0Xb0ae
H 9 0Xb19e
H 8 0Xb27a
H 7 0Xb342
H 6 0Xb414
H O 0Xb504
H 4 0Xb608
H 1 0Xb6da
H 3 0Xb766
H E 0Xb7f2
H T 0Xb8ba
B 5 0Xb9d2
B 2 0Xbaa4
B 9 0Xbb44
B 7 0Xbc0c
B 8 0Xbcc0
B O 0Xbdd8
B 6 0Xbeb4
B 1 0Xbf7c
B 4 0Xc06c
B E 0Xc10c
B 3 0Xc1de
B T 0Xc274
H 2 0Xc47c
H 5 0Xc512
H 7 0Xc620
H 9 0Xc684
H O 0Xc79c
H 8 0Xc864
H 1 0Xc95e
H 6 0Xc9fe
H E 0Xcaa8
H 4 0Xcb7a
H 3 0Xcc42
H T 0Xccf6
B 2 0Xcdfa
B 7 0Xcee0
B 5 0Xcfd0
B O 0Xd08e
B 9 0Xd12e
B 1 0Xd21e
B 8 0Xd2aa
B E 0Xd3a4
B 6 0Xd476
B 3 0Xd548
B 4 0Xd61a
B T 0Xd6b0
H 7 0Xd89a
H 2 0Xd958
H 5 0Xda0c
H 9 0Xdb10
H O 0Xdbb0
H 8 0Xdca0
H 1 0Xdd54
H 6 0Xde12
H E 0Xdec6
H 4 0Xdf84
H 3 0Xe060
H T 0Xe146
B 7 0Xe236
B 5 0Xe31c
B 2 0Xe402
B O 0Xe498
B 9 0Xe59c
B 1 0Xe65a
B 8 0Xe6d2
B E 0Xe7e0
B 6 0Xe8a8
B 3 0Xe952
B 4 0Xea10
B T 0Xeace
H 5 0Xecb8
H 7 0Xedd0
H 2 0Xee84
H 9 0Xef24
H O 0Xeff6
H 8 0Xf0d2
H 1 0Xf1a4
H 6 0Xf280
H E 0Xf2f8
H 4 0Xf3f2
H 3 0Xf49c
H T 0Xf564
B 5 0Xf640
B 2 0Xf74e
B 7 0Xf820
B O 0Xf8f2
B 9 0Xf9a6
B 1 0Xfa78
B 8 0Xfaf0
B E 0Xfbf4
B 6 0Xfd0c
B 3 0Xfdac
B 4 0Xfe7e
B T 0Xff1e
H 2 0X0108
H 5 0X01b2
H O 0X02b6
H 7 0X0360
H 1 0X046e
H 9 0X04e6
H E 0X05c2
H 8 0X06bc
H 3 0X0798
H 6 0X082e
H 4 0X090a
H T 0X09d2
B 5 0X0ab8
B 2 0X0bb2
B O 0X0c52
B 1 0X0d10
B 7 0X0dd8
B E 0X0eaa
B 9 0X0f68
B 3 0X1062
B 8 0X10f8
B 4 0X11d4
B 6 0X12a6
B T 0X1378
H 5 0X151c
H O 0X162a
H 2 0X16d4
H 7 0X17ba
H 1 0X188c
H 9 0X190e
H E 0X19e0
H 8 0X1ac6
H 3 0X1b84
H 6 0X1c60
H 4 0X1d3c
H T 0X1e0e
B O 0X1ed6
B 5 0X1fbc
B 2 0X20b6
B 1 0X2174
B 7 0X2232
B E 0X22dc
B 9 0X23ae
B 3 0X2462
B 8 0X2502
B 4 0X25e8
B 6 0X26b0
B T 0X27aa
H O 0X2962
H 2 0X2a52
H 5 0X2afc
H 7 0X2c46
H 1 0X2cdc
H 9 0X2d4a
H E 0X2e30
H 8 0X2f02
H 3 0X2fde
H 6 0X30a6
H 4 0X313c
H T 0X320e
B 2 0X3376
B O 0X33f8
B 5 0X34ac
B 1 0X359c
B 7 0X3682
B E 0X3736
B 9 0X37f4
B 3 0X38e4
B 8 0X398e
B 4 0X3a6a
B 6 0X3b0a
B T 0X3bc8
H O 0X3da8
H 2 0X3e7a
H 1 0X3f6a
H 5 0X400a
H E 0X40be
H 7 0X4186
H 3 0X428a
H 9 0X4352
H 4 0X4410
H 8 0X44e2
H 6 0X45a0
H T 0X4686
B O 0X474e
B 1 0X4816
B 2 0X48fc
B E 0X4992
B 5 0X4a82
B 3 0X4b68
B 7 0X4bfe
B 4 0X4d02
B 9 0X4dde
B 6 0X4e88
B 8 0X4f46
B T 0X502c
H 1 0X51f8
H O 0X52c0
H 2 0X5388
H 5 0X543c
H E 0X54fa
H 7 0X55d6
H 3 0X5694
H 9 0X5752
H 4 0X5874
H 8 0X591e
H 6 0X59e6
H T 0X5acc
B 1 0X5ba8
B 2 0X5c5c
B O 0X5d24
B E 0X5df6
B 5 0X5ea0
B 3 0X5f90
B 7 0X6058
B 4 0X6134
B 9 0X61fc
B 6 0X62ba
B 8 0X638c
B T 0X644a
H 2 0X6652
H 1 0X6738
H O 0X67b0
H 5 0X688c
H E 0X6918
H 7 0X6a1c
H 3 0X6aee
H 9 0X6b98
H 4 0X6c92
H 8 0X6d5a
H 6 0X6df0
H T 0X6efe
B 2 0X6fee
B O 0X70ac
B 1 0X7156
B E 0X7200
B 5 0X72c8
B 3 0X73cc
B 7 0X748a
B 4 0X752a
B 9 0X7638
B 6 0X76d8
B 8 0X77a0
B T 0X787c
H O 0X7a66
H 2 0X7b38
H E 0X7bd8
H 1 0X7ca0
H 3 0X7d4a
H 5 0X7e12
H 4 0X7f16
H 7 0X7ff2
H 6 0X80b0
H 9 0X813c
H 8 0X820e
H T 0X8308
B 2 0X8434
B O 0X84de
B E 0X85ba
B 3 0X865a
B 1 0X870e
B 4 0X87d6
B 5 0X888a
B 6 0X8970
B 7 0X8a7e
B 8 0X8b32
B 9 0X8bbe
B T 0X8c9a
H 2 0X8ea2
H E 0X8f38
H O 0X903c
H 1 0X90e6
H 3 0X919a
H 5 0X924e
H 4 0X932a
H 7 0X9410
H 6 0X94a6
H 9 0X956e
H 8 0X9654
H T 0X9730
B E 0X983e
B 2 0X991a
B O 0X99e2
B 3 0X9aa0
B 1 0X9b5e
B 4 0X9c44
B 5 0X9ce4
B 6 0X9d7a
B 7 0X9e60
B 8 0X9f00
B 9 0X9ff0
B T 0Xa0b8
H E 0Xa284
H O 0Xa39c
H 2 0Xa450
H 1 0Xa536
H 3 0Xa5f4
H 5 0Xa694
H 4 0Xa784
H 7 0Xa856
H 6 0Xa8b0
H 9 0Xa978
H 8 0Xaa4a
H T 0Xab44
B O 0Xac20
B E 0Xad42
B 2 0Xadf6
B 3 0Xaebe
B 1 0Xafae
B 4 0Xb076
B 5 0Xb0ee
B 6 0Xb1d4
B 7 0Xb288
B 8 0Xb35a
B 9 0Xb40e
B T 0Xb4d6
H E 0Xb6ac
H O 0Xb792
H 3 0Xb850
H 2 0Xb940
H 4 0Xba12
H 1 0Xbb16
H 6 0Xbb98
H 5 0Xbc4c
H 8 0Xbd14
H 7 0Xbdbe
H 9 0Xbe90
H T 0Xbf58
B E 0Xc0a2
B 3 0Xc12e
B O 0Xc1f6
B 4 0Xc2f0
B 2 0Xc3ae
B 6 0Xc48a
B 1 0Xc570
B 8 0Xc610
B 5 0Xc6ce
B 9 0Xc78c
B 7 0Xc84a
B T 0Xc8f4
H 3 0Xcb10
H E 0Xcbce
H O 0Xccaa
H 2 0Xcd9a
H 4 0Xce3a
H 1 0Xcf2a
H 6 0Xcfb6
H 5 0Xd092
H 8 0Xd146
H 7 0Xd236
H 9 0Xd2c2
H T 0Xd380
B 3 0Xd4b6
B O 0Xd56a
B E 0Xd66e
B 4 0Xd718
B 2 0Xd7ea
B 6 0Xd8a8
B 1 0Xd97a
B 8 0Xda1a
B 5 0Xdaf6
B 9 0Xdbb4
B 7 0Xdc7c
B T 0Xdd58
H O 0Xdf4c
H 3 0Xdfec
H E 0Xe0a0
H 2 0Xe1d6
H 4 0Xe276
H 1 0Xe370
H 6 0Xe406
H 5 0Xe4d8
H 8 0Xe596
H 7 0Xe67c
H 9 0Xe6f4
H T 0Xe7d0
B O 0Xe8ac
B E 0Xe9c4
B 3 0Xea8c
B 4 0Xeb72
B 2 0Xec3a
B 6 0Xecd0
B 1 0Xeda2
B 8 0Xee6a
B 5 0Xef32
B 9 0Xf004
B 7 0Xf0e0
B T 0Xf18a
H E 0Xf36a
H O 0Xf41e
H 4 0Xf504
H 3 0Xf5cc
H 6 0Xf6b2
H 2 0Xf7ac
H 8 0Xf84c
H 1 0Xf932
H 9 0Xf9d2
H 5 0Xfa9a
H 7 0Xfb94
H T 0Xfc2a
B O 0Xfcfc
B E 0Xfde2
B 4 0Xfedc
B 6 0Xff72
B 3 0X006c
B 8 0X012a
B 2 0X0224
B 9 0X02ba
B 1 0X03be
B 7 0X045e
B 5 0X051c
B T 0X05ee
H O 0X0774
H 4 0X0864
H E 0X092c
H 3 0X0a26
H 6 0X0aee
H 2 0X0c10
H 8 0X0cb0
H 1 0X0d5a
H 9 0X0e18
H 5 0X0efe
H 7 0X0fe4
H T 0X1084
B 4 0X1142
B O 0X120a
B E 0X1322
B 6 0X13cc
B 3 0X14da
B 8 0X155c
B 2 0X164c
B 9 0X1700
B 1 0X17b4
B 7 0X18ae
B 5 0X1980
B T 0X1a5c
H 4 0X1bec
H E 0X1ca0
H O 0X1d72
H 3 0X1e80
H 6 0X1f48
H 2 0X204c
H 8 0X2100
H 1 0X2196
H 9 0X2240
H 5 0X2330
H 7 0X2420
H T 0X24c0
B E 0X25ce
B 4 0X268c
B O 0X2740
B 6 0X2826
B 3 0X2916
B 8 0X29c0
B 2 0X2aba
B 9 0X2b6e
B 1 0X2bfa
B 7 0X2cea
B 5 0X2dc6
B T 0X2e7a
H 4 0X306e
H E 0X312c
H 6 0X31e0
H O 0X329e
H 8 0X3384
H 3 0X346a
H 9 0X351e
H 2 0X3618
H 7 0X36cc
H 1 0X3780
H 5 0X382a
H T 0X3906
B 4 0X39ba
B 6 0X3aaa
B E 0X3b90
B 8 0X3c4e
B O 0X3d2a
B 9 0X3e1a
B 3 0X3ec4
B 7 0X3fbe
B 2 0X4090
B 5 0X413a
B 1 0X4216
B T 0X42c0
H 6 0X4478
H 4 0X4586
H E 0X4608
H O 0X46e4
H 8 0X47b6
H 3 0X4892
H 9 0X4950
H 2 0X4a7c
H 7 0X4b3a
H 1 0X4c02
H 5 0X4c70
H T 0X4d60
B 6 0X4e3c
B E 0X4efa
B 4 0X4fe0
B 8 0X506c
B O 0X5170
B 9 0X5242
B 3 0X5300
B 7 0X5422
B 2 0X54e0
B 5 0X559e
B 1 0X5648
B T 0X5710
H E 0X58be
H 6 0X59b8
H 4 0X5a9e
H O 0X5b66
H 8 0X5c06
H 3 0X5d0a
H 9 0X5d96
H 2 0X5e90
H 7 0X5f62
H 1 0X6016
H 5 0X60ca
H T 0X619c
B E 0X62aa
B 4 0X6368
B 6 0X6412
B 8 0X64f8
B O 0X65d4
B 9 0X6692
B 3 0X6746
B 7 0X6840
B 2 0X68d6
B 5 0X69b2
B 1 0X6a7a
B T 0X6b56
H 4 0X6d0e
H E 0X6dea
H 8 0X6ed0
H 6 0X6f84
H 9 0X704c
H O 0X7146
H 7 0X720e
H 3 0X72c2
H 5 0X7362
H 2 0X743e
H 1 0X74fc
H T 0X75d8
B E 0X76dc
B 4 0X7786
B 8 0X7844
B 9 0X7934
B 6 0X79e8
B 7 0X7aec
B O 0X7baa
B 5 0X7c86
B 3 0X7d30
B 1 0X7de4
B 2 0X7e8e
B T 0X7f56
H E 0X812c
H 8 0X8208
H 4 0X8316
H 6 0X83d4
H 9 0X8474
H O 0X856e
H 7 0X862c
H 3 0X86f4
H 5 0X87a8
H 2 0X8884
H 1 0X8942
H T 0X89f6
B 8 0X8ad2
B E 0X8bc2
B 4 0X8ca8
B 9 0X8d52
B 6 0X8e2e
B 7 0X8f1e
B O 0X8fc8
B 5 0X9086
B 3 0X916c
B 1 0X9202
B 2 0X92b6
B T 0X93b0
H 8 0X9586
H 4 0X963a
H E 0X96ee
H 6 0X97f2
H 9 0X98ba
H O 0X9996
H 7 0X9a68
H 3 0X9b12
H 5 0X9bc6
H 2 0X9cb6
H 1 0X9d92
H T 0X9e32
B 4 0X9ef0
B 8 0X9fb8
B E 0Xa0bc
B 9 0Xa17a
B 6 0Xa26a
B 7 0Xa35a
B O 0Xa42c
B 5 0Xa4cc
B 3 0Xa56c
B 1 0Xa63e
B 2 0Xa6fc
B T 0Xa7ba
H 8 0Xa986
H 4 0Xaa58
H 9 0Xab02
H E 0Xac10
H 7 0Xad00
H 6 0Xadbe
H 5 0Xae86
H O 0Xaf80
H 1 0Xb05c
H 3 0Xb0d4
H 2 0Xb192
H T 0Xb228
B 8 0Xb2fa
B 9 0Xb3f4
B 4 0Xb4c6
B 7 0Xb5ca
B E 0Xb67e
B 5 0Xb750
B 6 0Xb82c
B 1 0Xb8fe
B O 0Xb9c6
B 2 0Xbaa2
B 3 0Xbb60
B T 0Xbbce
H 9 0Xbdea
H 8 0Xbea8
H 4 0Xbf70
H E 0Xc010
H 7 0Xc11e
H 6 0Xc1c8
H 5 0Xc290
H O 0Xc380
H 1 0Xc448
H 3 0Xc506
H 2 0Xc5c4
H T 0Xc65a
B 9 0Xc754
B 4 0Xc84e
B 8 0Xc8da
B 7 0Xc9d4
B E 0Xca9c
B 5 0Xcb64
B 6 0Xcc36
B 1 0Xccfe
B O 0Xcdd0
B 2 0Xceb6
B 3 0Xcf56
B T 0Xd014
H 4 0Xd208
H 9 0Xd2b2
H 8 0Xd37a
H E 0Xd474
H 7 0Xd546
H 6 0Xd604
H 5 0Xd6e0
H O 0Xd7b2
H 1 0Xd848
H 3 0Xd8fc
H 2 0Xd9f6
H T 0Xda96
B 4 0Xdb86
B 8 0Xdc44
B 9 0Xdd20
B 7 0Xde24
B E 0Xdee2
B 5 0Xdfa0
B 6 0Xe090
B 1 0Xe162
B O 0Xe22a
B 2 0Xe2e8
B 3 0Xe36a
B T 0Xe43c
H 8 0Xe630
H 4 0Xe6da
H 7 0Xe7fc
H 9 0Xe888
H 5 0Xe950
H E 0Xea04
H 1 0Xeb3a
H 6 0Xebda
H 2 0Xecde
H O 0Xed9c
H 3 0Xee28
H T 0Xeef0
B 4 0Xefd6
B 8 0Xf080
B 7 0Xf166
B 5 0Xf256
B 9 0Xf314
B 1 0Xf3fa
B E 0Xf490
B 2 0Xf59e
B 6 0Xf65c
B 3 0Xf706
B O 0Xf7ec
B T 0Xf896
H 4 0Xfa76
H 7 0Xfb5c
H 8 0Xfbf2
H 9 0Xfcce
H 5 0Xfda0
H E 0Xfe86
H 1 0Xff44
H 6 0Xffe4
H 2 0X00b6
H O 0X01b0
H 3 0X025a
H T 0X032c
B 7 0X041c
B 4 0X04e4
B 8 0X0584
B 5 0X0674
B 9 0X075a
B 1 0X082c
B E 0X0908
B 2 0X0994
B 6 0X0a52
B 3 0X0b42
B O 0X0c28
B T 0X0ce6
H 7 0X0eb2
H 8 0X0f70
H 4 0X1042
H 9 0X110a
H 5 0X11d2
H E 0X12c2
H 1 0X136c
H 6 0X143e
H 2 0X1510
H O 0X1600
H 3 0X16b4
H T 0X174a
B 8 0X1876
B 7 0X1934
B 4 0X1a1a
B 5 0X1ace
B 9 0X1b8c
B 1 0X1c5e
B E 0X1d1c
B 2 0X1df8
B 6 0X1e8e
B 3 0X1f92
B O 0X2064
B T 0X2122
H 7 0X22ee
H 8 0X23ac
H 5 0X2474
H 4 0X255a
H 1 0X2636
H 9 0X2712
H 2 0X27ee
H E 0X287a
H 6 0X292e
H O 0X2a28
H 3 0X2afa
H T 0X2ba4
B 7 0X2ca8
B 5 0X2d66
B 8 0X2e10
B 1 0X2ef6
B 4 0X2fd2
B 2 0X30b8
B 9 0X316c
B 6 0X3220
B E 0X32fc
B 3 0X33e2
B O 0X3496
B T 0X3554
H 5 0X3720
H 7 0X3824
H 8 0X38c4
H 4 0X39aa
H 1 0X3a68
H 9 0X3b08
H 2 0X3bda
H E 0X3cac
H 6 0X3d88
H O 0X3e6e
H 3 0X3f0e
H T 0X3fe0
B 5 0X40c6
B 8 0X4198
B 7 0X42a6
B 1 0X4364
B 4 0X43f0
B 2 0X44d6
B 9 0X458a
B 6 0X4666
B E 0X4756
B 3 0X47d8
B O 0X48c8
B T 0X4990
H 8 0X4b84
H 5 0X4c38
H 7 0X4d32
H 4 0X4de6
H 1 0X4e7c
H 9 0X4f4e
H 2 0X5016
H E 0X5106
H 6 0X51d8
H O 0X5282
H 3 0X535e
H T 0X5430
B 8 0X5502
B 7 0X5606
B 5 0X56c4
B 1 0X5778
B 4 0X5836
B 2 0X591c
B 9 0X59c6
B 6 0X5aca
B E 0X5ba6
B 3 0X5c6e
B O 0X5d2c
B T 0X5dd6
H 7 0X5fd4
H 8 0X6088
H 1 0X6164
H 5 0X620e
H 2 0X62ea
H 4 0X63da
H 6 0X6498
H 9 0X652e
H 3 0X6628
H E 0X66b4
H O 0X6790
H T 0X686c
B 8 0X697a
B 7 0X6a60
B 1 0X6b28
B 2 0X6c04
B 5 0X6c7c
B 6 0X6d62
B 4 0X6e52
B 3 0X6efc
B 9 0X6fba
B O 0X7078
B E 0X714a
B T 0X7212
H 8 0X7406
H 1 0X74f6
H 7 0X75be
H 5 0X7668
H 2 0X774e
H 4 0X77ee
H 6 0X78ca
H 9 0X796a
H 3 0X7a5a
H E 0X7af0
H O 0X7bb8
H T 0X7c80
B 1 0X7d8e
B 8 0X7e60
B 7 0X7f82
B 2 0X804a
B 5 0X80e0
B 6 0X8194
B 4 0X8298
B 3 0X8342
B 9 0X840a
B O 0X84a0
B E 0X859a
B T 0X863a
H 1 0X8806
H 7 0X88ec
H 8 0X89a0
H 5 0X8a9a
H 2 0X8b8a
H 4 0X8c70
H 6 0X8d10
H 9 0X8dc4
H 3 0X8e6e
H E 0X8f36
H O 0X9008
H T 0X90e4
B 7 0X91e8
B 1 0X927e
B 8 0X9328
B 2 0X9454
B 5 0X94f4
B 6 0X95d0
B 4 0X96ac
B 3 0X9774
B 9 0X9832
B O 0X9904
B E 0X99d6
B T 0X9a80
H 1 0X9c7e
H 7 0X9d3c
H 2 0X9df0
H 8 0X9ec2
H 6 0X9fa8
H 5 0Xa066
H 3 0Xa11a
H 4 0Xa1d8
H O 0Xa28c
H 9 0Xa35e
H E 0Xa43a
H T 0Xa50c
B 1 0Xa5d4
B 2 0Xa692
B 7 0Xa796
B 6 0Xa872
B 8 0Xa926
B 3 0Xa9ee
B 5 0Xaaac
B O 0Xab9c
B 4 0Xac50
B E 0Xace6
B 9 0Xadf4
B T 0Xaea8
H 2 0Xb088
H 1 0Xb164
H 7 0Xb218
H 8 0Xb2d6
H 6 0Xb3bc
H 5 0Xb4a2
H 3 0Xb542
H 4 0Xb63c
H O 0Xb6d2
H 9 0Xb786
H E 0Xb858
H T 0Xb90c
B 2 0Xba1a
B 7 0Xbaec
B 1 0Xbbaa
B 6 0Xbc86
B 8 0Xbd44
B 3 0Xbe20
B 5 0Xbede
B O 0Xbfa6
B 4 0Xc08c
B E 0Xc140
B 9 0Xc21c
B T 0Xc2c6
H 7 0Xc4b0
H 2 0Xc582
H 1 0Xc668
H 8 0Xc74e
H 6 0Xc7d0
H 5 0Xc8b6
H 3 0Xc992
H 4 0Xca64
H O 0Xcadc
H 9 0Xcb9a
H E 0Xcc80
H T 0Xcd52
B 7 0Xce74
B 1 0Xcf14
B 2 0Xcfc8
B 6 0Xd0d6
B 8 0Xd194
B 3 0Xd252
B 5 0Xd310
B O 0Xd3d8
B 4 0Xd4d2
B E 0Xd5c2
B 9 0Xd630
B T 0Xd6ee
H 1 0Xd8ce
H 7 0Xd9c8
H 6 0Xda68
H 2 0Xdb6c
H 3 0Xdc16
H 8 0Xdcfc
H O 0Xddce
H 5 0Xde6e
H 4 0Xdf4a
H E 0Xdfc2
H 9 0Xe0da
H T 0Xe198
B 7 0Xe29c
B 1 0Xe332
B 6 0Xe422
B 3 0Xe4fe
B 2 0Xe5c6
B O 0Xe67a
B 8 0Xe74c
B 4 0Xe80a
B 5 0Xe8dc
B 9 0Xe99a
B E 0Xea94
B T 0Xeb34
H 7 0Xed64
H 6 0Xedd2
H 1 0Xeed6
H 2 0Xefa8
H 3 0Xf052
H 8 0Xf11a
H O 0Xf1ce
H 5 0Xf2b4
H 4 0Xf386
H E 0Xf41c
H 9 0Xf520
H T 0Xf5c0
B 6 0Xf6b0
B 7 0Xf7c8
B 1 0Xf890
B 3 0Xf93a
B 2 0Xf9e4
B O 0Xfab6
B 8 0Xfb42
B 4 0Xfc14
B 5 0Xfcfa
B 9 0Xfde0
B E 0Xfebc
B T 0Xff8e
H 6 0X013c
H 1 0X0268
H 7 0X0312
H 2 0X03f8
H 3 0X0498
H 8 0X054c
H O 0X0628
H 5 0X06d2
H 4 0X0772
H E 0X0844
H 9 0X0916
H T 0X09de
B 1 0X0af6
B 6 0X0be6
B 7 0X0cf4
B 3 0X0d8a
B 2 0X0e3e
B O 0X0efc
B 8 0X0f92
B 4 0X1082
B 5 0X1122
B 9 0X1212
B E 0X12e4
B T 0X1398
H 6 0X1550
H 1 0X165e
H 3 0X1726
H 7 0X183e
H O 0X18e8
H 2 0X1974
H 4 0X1a3c
H 8 0X1b22
H 9 0X1bc2
H 5 0X1cb2
H E 0X1d48
H T 0X1e10
B 6 0X1f1e
B 3 0X1ffa
B 1 0X20f4
B O 0X219e
B 7 0X223e
B 4 0X2342
B 2 0X2400
B 9 0X24aa
B 8 0X257c
B E 0X2626
B 5 0X272a
B T 0X27d4
H 3 0X29c8
H 6 0X2a7c
H 1 0X2b6c
H 7 0X2c20
H O 0X2cde
H 2 0X2db0
H 4 0X2e6e
H 8 0X2f54
H 9 0X3008
H 5 0X30ee
H E 0X3170
H T 0X322e
B 3 0X3346
B 1 0X3436
B 6 0X34e0
B O 0X35b2
B 7 0X3666
B 4 0X3760
B 2 0X37f6
B 9 0X38dc
B 8 0X3986
B E 0X3a76
B 5 0X3b84
B T 0X3c2e
H 1 0X3dfa
H 3 0X3eae
H 6 0X3f76
H 7 0X405c
H O 0X40fc
H 2 0X41ba
H 4 0X428c
H 8 0X4354
H 9 0X4426
H 5 0X4520
H E 0X45b6
H T 0X4692
B 1 0X476e
B 6 0X480e
B 3 0X4908
B O 0X49c6
B 7 0X4a70
B 4 0X4b92
B 2 0X4c32
B 9 0X4cfa
B 8 0X4da4
B E 0X4e8a
B 5 0X4f8e
B T 0X5056
H 6 0X5204
H 1 0X52e0
H O 0X53c6
H 3 0X5470
H 4 0X5542
H 7 0X5600
H 9 0X56be
H 2 0X577c
H 8 0X57fe
H E 0X58da
H 5 0X5a06
H T 0X5ad8
B 1 0X5b96
B 6 0X5c68
B O 0X5d44
B 4 0X5e16
B 3 0X5ee8
B 9 0X5f88
B 7 0X6050
B 8 0X60f0
B 2 0X61ea
B 5 0X62d0
B E 0X6384
B T 0X6456
H 1 0X6636
H O 0X671c
H 6 0X678a
H 3 0X688e
H 4 0X6938
H 7 0X6a46
H 9 0X6ae6
H 2 0X6bc2
H 8 0X6c62
H E 0X6d34
H 5 0X6e2e
H T 0X6ece
B O 0X6fc8
B 1 0X7090
B 6 0X7130
B 4 0X7248
B 3 0X7306
B 9 0X73a6
B 7 0X7482
B 8 0X7536
B 2 0X7612
B 5 0X7716
B E 0X77de
B T 0X7892
H O 0X7a68
H 6 0X7b08
H 1 0X7be4
H 3 0X7ca2
H 4 0X7d92
H 7 0X7e82
H 9 0X7f0e
H 2 0X7fd6
H 8 0X80c6
H E 0X818e
H 5 0X8242
H T 0X8314
B 6 0X83e6
B O 0X84c2
B 1 0X858a
B 4 0X8684
B 3 0X8756
B 9 0X87ec
B 7 0X88c8
B 8 0X89c2
B 2 0X8a26
B 5 0X8b16
B E 0X8be8
B T 0X8cd8
H O 0X8e7c
H 6 0X8f58
H 4 0X8fee
H 1 0X90fc
H 9 0X91ce
H 3 0X92c8
H 8 0X939a
H 7 0X9444
H 5 0X94ee
H 2 0X95d4
H E 0X9656
H T 0X9728
B O 0X9804
B 4 0X98ea
B 6 0X99bc
B 9 0X9a98
B 1 0X9b9c
B 8 0X9c64
B 3 0X9d5e
B 5 0X9dea
B 7 0X9ed0
B E 0X9f7a
B 2 0Xa06a
B T 0Xa0ce
H 4 0Xa29a
H O 0Xa358
H 6 0Xa466
H 1 0Xa556
H 9 0Xa5ec
H 3 0Xa6f0
H 8 0Xa7ae
H 7 0Xa89e
H 5 0Xa93e
H 2 0Xaa24
H E 0Xaa9c
H T 0Xab6e
B 4 0Xac90
B 6 0Xad44
B O 0Xae0c
B 9 0Xaeb6
B 1 0Xafa6
B 8 0Xb05a
B 3 0Xb136
B 5 0Xb208
B 7 0Xb2e4
B E 0Xb384
B 2 0Xb46a
B T 0Xb546
H 6 0Xb6fe
H 4 0Xb802
H O 0Xb898
H 1 0Xb974
H 9 0Xba28
H 3 0Xbb18
H 8 0Xbbc2
H 7 0Xbca8
H 5 0Xbd66
H 2 0Xbe6a
H E 0Xbeba
H T 0Xbf8c
B 6 0Xc090
B O 0Xc176
B 4 0Xc25c
B 9 0Xc306
B 1 0Xc3d8
B 8 0Xc496
B 3 0Xc5ae
B 5 0Xc662
B 7 0Xc748
B E 0Xc7c0
B 2 0Xc8b0
B T 0Xc964
H O 0Xcb26
H 6 0Xcc16
H 9 0Xccde
H 4 0Xcdc4
H 8 0Xce82
H 1 0Xcf0e
H 5 0Xcfe0
H 3 0Xd0d0
H E 0Xd17a
H 7 0Xd2c4
H 2 0Xd38c
H T 0Xd3f0
B 6 0Xd4ea
B O 0Xd5a8
B 9 0Xd6ac
B 8 0Xd76a
B 4 0Xd832
B 5 0Xd8e6
B 1 0Xd990
B E 0Xda6c
B 3 0Xdb5c
B 2 0Xdc24
B 7 0Xdd28
B T 0Xddd2
H 6 0Xdf62
H 9 0Xe048
H O 0Xe124
H 4 0Xe1d8
H 8 0Xe2be
H 1 0Xe39a
H 5 0Xe444
H 3 0Xe502
H E 0Xe5a2
H 7 0Xe67e
H 2 0Xe778
H T 0Xe84a
B 9 0Xe93a
B 6 0Xe9d0
B O 0Xeaca
B 8 0Xeb60
B 4 0Xec78
B 5 0Xed2c
B 1 0Xedd6
B E 0Xeea8
B 3 0Xefa2
B 2 0Xf060
B 7 0Xf13c
B T 0Xf1c8
H 9 0Xf3d0
H O 0Xf484
H 6 0Xf538
H 4 0Xf600
H 8 0Xf6dc
H 1 0Xf7e0
H 5 0Xf894
H 3 0Xf97a
H E 0Xfa06
H 7 0Xfae2
H 2 0Xfbc8
H T 0Xfc5e
B O 0Xfd44
B 9 0Xfe34
B 6 0Xff10
B 8 0Xffd8
B 4 0X0096
B 5 0X0168
B 1 0X026c
B E 0X0320
B 3 0X03fc
B 2 0X04c4
B 7 0X05b4
B T 0X062c
H 9 0X07d0
H O 0X08ca
H 8 0X0992
H 6 0X0a8c
H 5 0X0b54
H 4 0X0c44
H E 0X0cf8
H 1 0X0dd4
H 2 0X0e6a
H 3 0X0f1e
H 7 0X1004
H T 0X10cc
B 9 0X119e
B 8 0X1266
B O 0X1356
B 5 0X145a
B 6 0X150e
B E 0X15e0
B 4 0X16a8
B 2 0X1766
B 1 0X182e
B 7 0X190a
B 3 0X19aa
B T 0X1a68
H 8 0X1c66
H 9 0X1d10
H O 0X1e0a
H 6 0X1eb4
H 5 0X1f90
H 4 0X2062
H E 0X2116
H 1 0X21f2
H 2 0X2274
H 3 0X2378
H 7 0X2468
H T 0X2508
B 8 0X25da
B O 0X26d4
B 9 0X27a6
B 5 0X2878
B 6 0X2936
B E 0X2a12
B 4 0X2a9e
B 2 0X2b84
B 1 0X2c6a
B 7 0X2d3c
B 3 0X2e18
B T 0X2eb8
H O 0X3070
H 8 0X3142
H 9 0X320a
H 6 0X32dc
H 5 0X33cc
H 4 0X343a
H E 0X3534
H 1 0X362e
H 2 0X3674
H 3 0X378c
H 7 0X3854
H T 0X3912
B O 0X3a16
B 9 0X3ade
B 8 0X3bb0
B 5 0X3ca0
B 6 0X3d5e
B E 0X3e4e
B 4 0X3ee4
B 2 0X3f8e
B 1 0X407e
B 7 0X413c
B 3 0X420e
B T 0X42b8
H 9 0X4470
H O 0X4542
H 5 0X4632
H 8 0X470e
H E 0X47ea
H 6 0X48bc
H 2 0X49c0
H 4 0X4a56
H 7 0X4b32
H 1 0X4c0e
H 3 0X4c5e
H T 0X4d26
B O 0X4e2a
B 9 0X4ee8
B 5 0X500a
B E 0X50aa
B 8 0X5186
B 2 0X528a
B 6 0X5320
B 7 0X5410
B 4 0X54d8
B 3 0X55b4
B 1 0X567c
B T 0X571c
H O 0X58a2
H 5 0X59a6
H 9 0X5a64
H 8 0X5b40
H E 0X5bfe
H 6 0X5cda
H 2 0X5dca
H 4 0X5eba
H 7 0X5f96
H 1 0X6022
H 3 0X60ea
H T 0X61c6
B 5 0X6298
B O 0X634c
B 9 0X6400
B E 0X64f0
B 8 0X65f4
B 2 0X66bc
B 6 0X6748
B 7 0X6856
B 4 0X691e
B 3 0X69d2
B 1 0X6aae
B T 0X6b76
H 5 0X6d1a
H 9 0X6dba
H O 0X6ea0
H 8 0X6f86
H E 0X704e
H 6 0X710c
H 2 0X71e8
H 4 0X72e2
H 7 0X73c8
H 1 0X7486
H 3 0X7526
H T 0X75e4
B 9 0X76de
B 5 0X779c
B O 0X785a
B E 0X794a
B 8 0X7a30
B 2 0X7af8
B 6 0X7b98
B 7 0X7c74
B 4 0X7d28
B 3 0X7e18
B 1 0X7ef4
B T 0X7f9e
H 5 0X8156
H 9 0X821e
H E 0X82f0
H O 0X83d6
H 2 0X84d0
H 8 0X858e
H 7 0X866a
H 6 0X8728
H 3 0X87e6
H 4 0X8872
H 1 0X8976
H T 0X8a20
B 5 0X8b1a
B E 0X8bec
B 9 0X8caa
B 2 0X8da4
B O 0X8e62
B 7 0X8f02
B 8 0X8fac
B 3 0X90b0
B 6 0X9178
B 1 0X924a
B 4 0X92f4
B T 0X93bc
H E 0X95b0
H 5 0X9678
H 9 0X972c
H O 0X9826
H 2 0X98e4
H 8 0X9984
H 7 0X9a60
H 6 0X9b50
H 3 0X9c18
H 4 0X9cf4
H 1 0X9d76
H T 0X9e52
B E 0X9f60
B 9 0Xa028
B 5 0Xa0e6
B 2 0Xa1e0
B O 0Xa29e
B 7 0Xa316
B 8 0Xa3d4
B 3 0Xa4f6
B 6 0Xa582
B 1 0Xa640
B 4 0Xa730
B T 0Xa816
H 9 0Xa9c4
H E 0Xaaa0
H 5 0Xab4a
H O 0Xac4e
H 2 0Xad02
H 8 0Xadca
H 7 0Xaeba
H 6 0Xaf50
H 3 0Xb04a
H 4 0Xb144
H 1 0Xb1ee
H T 0Xb284
B 9 0Xb34c
B 5 0Xb464
B E 0Xb504
B 2 0Xb612
B O 0Xb6bc
B 7 0Xb75c
B 8 0Xb81a
B 3 0Xb928
B 6 0Xb9f0
B 1 0Xbac2
B 4 0Xbba8
B T 0Xbc52
H 5 0Xbe0a
H 9 0Xbec8
H 2 0Xbfb8
H E 0Xc080
H 7 0Xc15c
H O 0Xc21a
H 3 0Xc2ec
H 8 0Xc3be
H 1 0Xc4b8
H 6 0Xc544
H 4 0Xc5e4
H T 0Xc6ca
B 9 0Xc7ba
B 5 0Xc8b4
B 2 0Xc986
B 7 0Xca30
B E 0Xcaf8
B 3 0Xcbd4
B O 0Xcc88
B 1 0Xcd96
B 8 0Xce4a
B 4 0Xceea
B 6 0Xcfd0
B T 0Xd08e
H 9 0Xd23c
H 2 0Xd35e
H 5 0Xd3f4
H E 0Xd638
H 7 0Xd656
H O 0Xd66a
H 3 0Xd746
H 8 0Xd80e
H 1 0Xd8d6
H 6 0Xd9a8
H 4 0Xda3e
H T 0Xdb24
B 2 0Xdc0a
B 9 0Xdcaa
B 5 0Xdd86
B 7 0Xdf16
B E 0Xdfc0
B 3 0Xe060
B O 0Xe0f6
B 1 0Xe18c
B 8 0Xe290
B 4 0Xe330
B 6 0Xe402
B T 0Xe4de
H 2 0Xe678
H 5 0Xe736
H 9 0Xe81c
H E 0Xe902
H 7 0Xea1a
H O 0Xead8
H 3 0Xeb78
H 8 0Xec5e
H 1 0Xece0
H 6 0Xedda
H 4 0Xee48
H T 0Xef4c
B 5 0Xf046
B 2 0Xf136
B 9 0Xf1d6
B 7 0Xf2a8
B E 0Xf384
B 3 0Xf474
B O 0Xf528
B 1 0Xf5a0
B 8 0Xf6a4
B 4 0Xf758
B 6 0Xf87a
B T 0Xf910
H 2 0Xfac8
H 5 0Xfb90
H 7 0Xfc9e
H 9 0Xfd52
H 3 0Xfe24
H E 0Xfea6
H 1 0Xff3c
H O 0X0054
H 4 0X0144
H 8 0X0202
H 6 0X0310
H T 0X03a6
B 2 0X048c
B 7 0X0568
B 5 0X061c
B 3 0X0702
B 9 0X0798
B 1 0X07a2
B E 0X090a
B 4 0X09fa
B O 0X0ab8
B 6 0X0bc6
B 8 0X0c8e
B T 0X0d56
H 2 0X1044
H 5 0X10bc
H 7 0X1120
H 9 0X11b6
H 3 0X1256
H E 0X12ec
H 1 0X1382
H O 0X1472
H 4 0X153a
H 8 0X1620
H 6 0X16fc
H T 0X17ce
B 5 0X19fe
B 2 0X1a08
B 7 0X1a30
B 3 0X1b5c
B 1 0X1c56
B 9 0X1c56
B E 0X1d6e
B 4 0X1e22
B O 0X1ec2
B 6 0X2002
B 8 0X20a2
B T 0X2160
H 5 0X2354
H 7 0X23cc
H 2 0X2412
H 9 0X2584
H 3 0X2660
H E 0X2732
H 1 0X27f0
H O 0X2868
H 4 0X28fe
H 8 0X2a16
H 6 0X2afc
H T 0X2bba
B 5 0X2ce6
B 2 0X2cf0
B 7 0X2e80
B 3 0X2f20
B 9 0X3006
B 1 0X30ba
B E 0X316e
B 4 0X3218
B O 0X32d6
B 6 0X33bc
B 8 0X348e
B T 0X356a
H 2 0X3722
H 5 0X37ea
H 3 0X38d0
H 7 0X39ac
H 1 0X3a6a
H 9 0X3b3c
H 4 0X3bdc
H E 0X3c86
H 6 0X3d26
H O 0X3e16
H 8 0X3f06
H T 0X3fd8
B 5 0X40b4
B 2 0X419a
B 3 0X4244
B 1 0X42f8
B 7 0X43de
B 4 0X44ba
B 9 0X456e
B 6 0X4636
B E 0X4712
B 8 0X47c6
B O 0X4898
B T 0X496a
H 5 0X4afa
H 3 0X4be0
H 2 0X4cb2
H 7 0X4da2
H 1 0X4e74
H 9 0X4f32
H 4 0X4ffa
H E 0X50c2
H 6 0X5162
H O 0X5202
H 8 0X52fc
H T 0X53e2
B 3 0X54dc
B 5 0X5586
B 2 0X5658
B 1 0X5720
B 7 0X57e8
B 4 0X58ce
B 9 0X598c
B 6 0X5a54
B E 0X5b26
B 8 0X5c02
B O 0X5ca2
B T 0X5d7e
H 3 0X5f2c
H 2 0X5ff4
H 5 0X609e
H 7 0X61d4
H 1 0X629c
H 9 0X636e
H 4 0X63dc
H E 0X64d6
H 6 0X659e
H O 0X6666
H 8 0X6742
H T 0X67e2
B 2 0X68e6
B 3 0X699a
B 5 0X6a62
B 1 0X6b3e
B 7 0X6bf2
B 4 0X6ca6
B 9 0X6da0
B 6 0X6e68
B E 0X6f30
B 8 0X7016
B O 0X70f2
B T 0X71a6
H 3 0X7368
H 2 0X7426
H 1 0X74bc
H 5 0X758e
H 4 0X76a6
H 7 0X7728
H 6 0X782c
H 9 0X78b8
H 8 0X79ee
H E 0X7a8e
H O 0X7b60
H T 0X7c1e
B 3 0X7ce6
B 1 0X7d7c
B 2 0X7e62
B 4 0X7f16
B 5 0X7fe8
B 6 0X80ba
B 7 0X8178
B 8 0X824a
B 9 0X8330
B O 0X83da
B E 0X84de
B T 0X85b0
H 1 0X8768
H 3 0X8808
H 2 0X88bc
H 5 0X898e
H 4 0X8a42
H 7 0X8b14
H 6 0X8bdc
H 9 0X8c86
H 8 0X8d76
H E 0X8e52
H O 0X8f2e
H T 0X900a
B 1 0X90b4
B 2 0X917c
B 3 0X9226
B 4 0X9302
B 5 0X93c0
B 6 0X947e
B 7 0X9546
B 8 0X960e
B 9 0X96ea
B O 0X97da
B E 0X98a2
B T 0X996a
H 1 0X9b68
H 2 0X9bf4
H 3 0X9cb2
H 4 0X9d34
H 5 0X9e06
H 6 0X9ed8
H 7 0X9f8c
H 8 0Xa068
H 9 0Xa130
H O 0Xa22a
H E 0Xa2b6
H T 0Xa392
B 1 0Xa4aa
B 2 0Xa568
B 3 0Xa612
B 4 0Xa6da
B 5 0Xa7a2
B 6 0Xa824
B 7 0Xa91e
B 8 0Xa9e6
B 9 0Xaac2
B O 0Xab9e
B E 0Xac66
B T 0Xad06
H 1 0Xaf04
H 2 0Xafcc
H 3 0Xb06c
H 4 0Xb10c
H 5 0Xb1de
H 6 0Xb2ce
H 7 0Xb396
H 8 0Xb472
H 9 0Xb51c
H O 0Xb5f8
H E 0Xb6ac
H T 0Xb76a
B 1 0Xb878
B 2 0Xb94a
B 3 0Xb9f4
B 4 0Xba76
B 5 0Xbb2a
B 6 0Xbc1a
B 7 0Xbd0a
B 8 0Xbde6
B 9 0Xbeb8
B O 0Xbf94
B E 0Xc084
B T 0Xc17e
